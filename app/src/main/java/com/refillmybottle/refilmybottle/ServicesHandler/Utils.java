package com.refillmybottle.refilmybottle.ServicesHandler;

import android.app.VoiceInteractor;

import com.refillmybottle.refilmybottle.Maps.POJONearby.ClientNearby;
import com.refillmybottle.refilmybottle.Maps.SearchPlaces.GoogleMapAPI;

import okhttp3.Request;

/**
 * Created by abah on 13/07/18.
 */

public class Utils {

//    public static final String BASE_URL = "http://botol.dzen1010.ga/index.php/";
//    public static final String BASE_URL = "http://192.168.1.20/htdocs/magang/index.php/";
//    public static final String BASE_URL = "http://codelabs.ddns.net/htdocs/magang/index.php/";
//    public static final String BASE_URL = "http://192.168.43.182/htdocs/magang/index.php/";

//      public static final String BASE_URL = "http://192.168.43.112/COMMIT/index.php/";
//    public static final String BASE_URL = "http://192.168.1.99/COMMIT/index.php/";
    public static final String BASE_URL = "http://192.168.1.99/RefillMyBottle/index.php/";
//    public static final String BASE_URL = "http://192.168.43.112/RefillMyBottle/index.php/";
    public static RequestInterfaces getApiServices(){
        return RetrofitClient.getClient(BASE_URL).create(RequestInterfaces.class);
    }

    public static final String URL_MAPS = "https://maps.googleapis.com/maps/api/";
    public static GoogleMapAPI getAPINearby(){
        return ClientNearby.getClient(URL_MAPS).create(GoogleMapAPI.class);
    }
}
