package com.refillmybottle.refilmybottle.ServicesHandler;


import android.widget.CalendarView;

import com.facebook.stetho.inspector.network.ResponseBodyData;
import com.refillmybottle.refilmybottle.Event.POJODetailsEvent.ResponseDetailEvent;
import com.refillmybottle.refilmybottle.Event.ResponEventKlik;
import com.refillmybottle.refilmybottle.Event.ResponseLikesUnlikes;
import com.refillmybottle.refilmybottle.Event.listEventYear.ResponseGetEventList;
import com.refillmybottle.refilmybottle.Maps.MapsImagesPojo.ResponseMapsImg;
import com.refillmybottle.refilmybottle.Maps.POJODetailMaps.ResponseDetailsMaps;
import com.refillmybottle.refilmybottle.Maps.POJOREVIEW.ModelReview;
import com.refillmybottle.refilmybottle.Maps.responseRefill.responseRefill;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.ResponseDetailShop;
import com.refillmybottle.refilmybottle.Shop.ResponsePopular;
import com.refillmybottle.refilmybottle.newrefill.ResponseNew;
import com.refillmybottle.refilmybottle.response.RelationResponse;
import com.refillmybottle.refilmybottle.response.ResponseCity;
import com.refillmybottle.refilmybottle.response.ResponseCountry;
import com.refillmybottle.refilmybottle.response.ResponseFB;
import com.refillmybottle.refilmybottle.response.ResponseImagesMaps;
import com.refillmybottle.refilmybottle.response.ResponseMapList;
import com.refillmybottle.refilmybottle.response.ResponseRefresh;
import com.refillmybottle.refilmybottle.response.TypesResponse;
import com.refillmybottle.refilmybottle.response.response_state;
import com.refillmybottle.refilmybottle.statistik.POJOSTATISTIK.ResponseChart;
import com.refillmybottle.refilmybottle.statistik.ResponseRedeem;
import com.refillmybottle.refilmybottle.statistik.ResponseVoucher;

import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by abah on 13/07/18.
 */

public interface RequestInterfaces {
    @FormUrlEncoded
    @POST("water?do=login")
    Call<ResponseBody> loginRequest (
                        @Field("mail") String Username,
                        @Field("pass") String Password);

    @FormUrlEncoded
    @POST("water?do=register")
    Call<ResponseBody> registerrequest(
            @Field("namadpn") String namadpn,
            @Field("namablk") String namablk,
            @Field("mail") String mail,
            @Field("mail1") String mail1,
            @Field("pass") String pass,
            @Field("pass1") String pass1,
            @Field("tgl") String tgl,
            @Field("ngr") String ngr,
            @Field("prov") String prov,
            @Field("kot") String city,
            @Field("jln") String jln,
            @Field("img") String img
    );


    @POST("water?do=geo")
    Call<ResponseCountry> getCountry();

    @GET("water?do=relation")
    Call<RelationResponse> getRelationData();

    @FormUrlEncoded
    @POST("water?do=refresh")
    Call<ResponseRefresh> getRefreshProfile(@Field("token") String token);

    @GET("water?do=types")
    Call<TypesResponse> getTypesData();

    @FormUrlEncoded
    @POST("water?do=voucer")
    Call<ResponseVoucher> getVoucher(@Field("token") String token);

    @FormUrlEncoded
    @POST("water?do=voucer")
    Call<ResponseRedeem> RedeemVoucher(@Field("token") String token, @Field("id") String id);

    @FormUrlEncoded
    @POST("water?do=myvoucerlist")
    Call<ResponseVoucher> getMyVoucher(@Field("token") String token);

    @FormUrlEncoded
    @POST("water?do=geo")
    Call<response_state> getState(@Field("state") String State);

    @FormUrlEncoded
    @POST("water?do=geo")
    Call<ResponseCity> getCity(@Field("city") String city);


    @GET("water?do=shop")
    Call<ResponsePopular> getItemShop();

    @GET("water?do=topshop")
    Call<ResponsePopular> getItemPopular();

    @FormUrlEncoded
    @POST("water?do=map")
    Call<ResponseMapList> getMapNear(@Field("lon") Double lon, @Field("lat") Double lat, @Field("token") String token);

    @FormUrlEncoded
    @POST("water?do=logout")
    Call<ResponseBody> getLogout(@Field("token") String token);

    @FormUrlEncoded
    @POST("water?do=editmail")
    Call<ResponseBody> getEditmail(@Field("mailb") String Current, @Field("mail") String newMail, @Field("mail1") String confNewMail, @Field("pass") String pass);

    @FormUrlEncoded
    @POST("water?do=edpass")
    Call<ResponseBody> getEditpass(@Field("token") String token, @Field("pass") String Pass, @Field("psn") String PasNew, @Field("psn1") String PasConfNew);

    @FormUrlEncoded
    @POST("water?do=mapimg")
    Call<ResponseMapsImg> getImages(@Field("id") String id);

    @FormUrlEncoded
    @POST("water?do=mapdet")
    Call<ResponseDetailsMaps> getDetailMaps(@Field("id") String id);

    @FormUrlEncoded
    @POST("water?do=saved")
    Call<ResponseChart> getChartvalues(@Field("token") String token);

    @FormUrlEncoded
    @POST("water?do=updatepro")
    Call<ResponseBody> getUpdateProfile(@Field("token") String token,
                                        @Field("nama") String nama,
                                        @Field("tgl") String tgl,
                                        @Field("ngr") String ngr,
                                        @Field("prov") String prov,
                                        @Field("kot") String kot,
                                        @Field("jln") String jln,
                                        @Field("img") String img);

    @FormUrlEncoded
    @POST("water?do=review")
    Call<ResponseBody> PostReviewPlace(@Field("id") String id,
                                       @Field("token") String token,
                                       @Field("star") String star,
                                       @Field("komentar") String komentar);

    @FormUrlEncoded
    @POST("water?do=maprev")
    Call<ModelReview> getReview(@Field("id") String id);

    @FormUrlEncoded
    @POST("water?do=desho")
    Call<ResponseDetailShop> getDetailShop(@Field("id") String id, @Field("token") String token);

    @FormUrlEncoded
    @POST("water?do=shotd")
    Call<ResponEventKlik> getDetailEvent(@Field("token") String token, @Field("tgl") String tgl);

    @FormUrlEncoded
    @POST("water?do=newplace")
    Call<ResponseNew> newPlace(
            @Field("nama") String nama,
            @Field("alamat") String alamat,
            @Field("relasi") String relasi,
            @Field("tipe") String tipe,
            @Field("lon") String lon,
            @Field("lat") String lat,
            @Field("token") String token,
            @Field("img[]") List<String> img);

    @FormUrlEncoded
    @POST("water?do=etail")
    Call<ResponseDetailEvent> getEventMore(
            @Field("id") String id,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("water?do=refill")
    Call<responseRefill> getRefill(@Field("token") String token,
                                   @Field("id") String id);

    @FormUrlEncoded
    @POST("water?do=event-act")
    Call<ResponseLikesUnlikes> getLikesUnlikes(@Field("id") String id,
                                               @Field("todo") String todo,
                                               @Field("token") String token);
    @GET("water?do=showevent")
    Call<ResponseGetEventList> getEventLists();

    @FormUrlEncoded
    @POST("water?do=loginfb")
    Call<ResponseFB> getFBLogin(@Field("nama") String nama,
                                @Field("mail") String mail,
                                @Field("tgl") String tgl);
}
