package com.refillmybottle.refilmybottle.statistik;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyVoucher extends AppCompatActivity {

    @BindView(R.id.backMyVoucher)
    ImageView backMyVoucher;
    @BindView(R.id.titleMyVoucher)
    TextView titleMyVoucher;
    @BindView(R.id.recyclerMyVoucher)
    RecyclerView recyclerMyVoucher;

    RequestInterfaces mReq;
    SessionManager sessionManager;
    @BindView(R.id.notfounds)
    ImageView notfounds;
    @BindView(R.id.txtNotfounds)
    TextView txtNotfounds;
    private ArrayList<modelVoucher> data_voucher;
    private AdapterVoucher adapterVoucher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_voucher);
        ButterKnife.bind(this);
        mReq = Utils.getApiServices();
        sessionManager = new SessionManager(this);

        recyclerMyVoucher.setHasFixedSize(true);
        recyclerMyVoucher.setLayoutManager(new LinearLayoutManager(this));

        initViews();
    }

    private void initViews() {
        mReq.getMyVoucher(sessionManager.getSessionToken().toString()).enqueue(new Callback<ResponseVoucher>() {
            @Override
            public void onResponse(Call<ResponseVoucher> call, Response<ResponseVoucher> response) {

                    if (response.body().getStatus().equals("200")){
                        notfounds.setVisibility(View.GONE);
                        txtNotfounds.setVisibility(View.GONE);
                        ResponseVoucher responseVoucher = response.body();
                        data_voucher = new ArrayList<>(Arrays.asList(responseVoucher.getData()));
                        adapterVoucher = new AdapterVoucher(data_voucher);
                        recyclerMyVoucher.setAdapter(adapterVoucher);
                    } else {
                        notfounds.setVisibility(View.VISIBLE);
                        txtNotfounds.setVisibility(View.VISIBLE);
                        Toast.makeText(MyVoucher.this, response.body().getMsg().toString(), Toast.LENGTH_SHORT).show();
                    }


            }

            @Override
            public void onFailure(Call<ResponseVoucher> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.backMyVoucher)
    public void onViewClicked() {
        startActivity(new Intent(MyVoucher.this, Voucher.class));
        finish();
    }
}
