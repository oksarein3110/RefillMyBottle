package com.refillmybottle.refilmybottle.statistik;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.statistik.POJOSTATISTIK.ResponseChart;

import java.util.ArrayList;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class statitsticChartBar extends Fragment {


    public String TAG = "STATISTIK BOTOL";
    @BindView(R.id.titleRecentAct)
    TextView titleRecentAct;
    @BindView(R.id.backtoReward)
    TextView backtoReward;
    @BindView(R.id.barCharts)
    BarChart barCharts;
    SessionManager sessionManager;
    RequestInterfaces mReques;
    String token;
    Inflater inflater;
    View view;
    ViewGroup cont;


    ArrayList<BarEntry> BARENTRY;
    ArrayList<String> BarEntryLabels;
    BarDataSet Bardataset;
    BarData BARDATA;


    Unbinder unbinder;



    public static statitsticChartBar newInstance() {
        statitsticChartBar statitsticChartBar = new statitsticChartBar();
        return statitsticChartBar;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        mReques = Utils.getApiServices();

        View view = inflater.inflate(R.layout.activity_statitstic_chart_bar, container, false);
        sessionManager = new SessionManager(getContext());
        unbinder = ButterKnife.bind(this, view);

        BARENTRY = new ArrayList<>();
        BarEntryLabels = new ArrayList<String>();
        initViews();


        return view;
    }


    private void initViews() {


        mReques.getChartvalues(sessionManager.getSessionToken().toString()).enqueue(new Callback<ResponseChart>() {
            @Override
            public void onResponse(Call<ResponseChart> call, Response<ResponseChart> response) {
                ResponseChart responseChart = response.body();
                if (responseChart.getStatus().equals("200")){
                    for (int i = 0; i < responseChart.getPenggunaan().getMonthly().size(); i++) {
                        BARENTRY.add(new BarEntry(Float.parseFloat(responseChart.getPenggunaan().getMonthly().get(i).getBotolBln().toString()), i));
                        BarEntryLabels.add(responseChart.getPenggunaan().getMonthly().get(i).getBulan().toString());

                    }

                    Bardataset = new BarDataSet(BARENTRY, "Bottle");
                    BARDATA = new BarData(BarEntryLabels, Bardataset);
                    barCharts.setData(BARDATA);
                    Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
                    barCharts.setVisibleXRange(4f, 4f);
                    barCharts.setNoDataText("Tidak Dapat Menampilkan Data");
                    barCharts.animateY(3000);
                    XAxis xAxis = barCharts.getXAxis();
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                    barCharts.getAxisRight().setEnabled(false);
                    barCharts.setDescription("");
                    barCharts.getLegend().setEnabled(false);
                }


            }

            @Override
            public void onFailure(Call<ResponseChart> call, Throwable t) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.backtoReward)
    public void onViewClicked() {
        ((StatistikMainActivity)getActivity()).changeProgress();

    }




}
