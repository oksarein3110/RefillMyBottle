package com.refillmybottle.refilmybottle.statistik;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.rd.PageIndicatorView;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StatistikMainActivity extends AppCompatActivity {

    @BindView(R.id.pagerpoints)
    ViewPager pagerpoints;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.dailywaterIntake)
    TextView dailywaterIntake;
    @BindView(R.id.vouchertxt)
    TextView vouchertxt;
    @BindView(R.id.frameStatistik)
    FrameLayout frameStatistik;
    String point;
    String token;

    SessionManager sessionManager;
    @BindView(R.id.titleStatistic)
    TextView titleStatistic;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistik_main);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        pageIndicatorView.setCount(2);
        pageIndicatorView.setSelection(2);

        Bundle bund = getIntent().getExtras();
        point = bund.getString("poin");
        token = sessionManager.getSessionToken().toString();


        Fragment fragment = statitsticChartBar.newInstance();


        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameStatistik, fragment);
        fragmentTransaction.commit();

        SimpleFragmentAdapter adapter = new SimpleFragmentAdapter(this, getSupportFragmentManager());
        pagerpoints.setAdapter(adapter);
        pagerpoints.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageIndicatorView.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @OnClick({R.id.back_arrow, R.id.dailywaterIntake, R.id.vouchertxt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                finish();
                break;
            case R.id.dailywaterIntake:
                startActivity(new Intent(StatistikMainActivity.this, DailyWater.class));
                break;
            case R.id.vouchertxt:
                startActivity(new Intent(getApplication().getBaseContext(), Voucher.class));
                break;
        }
    }

    public void changeStatistik(){
        Fragment fragment = statitsticChartBar.newInstance();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameStatistik, fragment);
        fragmentTransaction.commit();
    }

    public void changeProgress(){
        Fragment fragment = ProgressReward.newInstance();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameStatistik, fragment);
        fragmentTransaction.commit();
    }

    class SimpleFragmentAdapter extends FragmentPagerAdapter {
        private Context mContext;

        public SimpleFragmentAdapter(Context context, FragmentManager fm) {
            super(fm);
            mContext = context;
        }


        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Bundle bund = new Bundle();
                bund.putString("point", point);
                Fragment fragment = new FragmentEarnedPoints();
                fragment.setArguments(bund);
                return fragment;
            } else {
                Fragment fragment = new FragmentBottleSaved();
                return fragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
