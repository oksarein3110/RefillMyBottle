package com.refillmybottle.refilmybottle.statistik.POJOSTATISTIK;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ResponseChart {


    @SerializedName("status")
    private String status;
    @SerializedName("penggunaan")
    private PenggunaanModelsAuto penggunaan;
    @SerializedName("msg")
    private String msg;

    public static ResponseChart objectFromData(String str) {

        return new Gson().fromJson(str, ResponseChart.class);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PenggunaanModelsAuto getPenggunaan() {
        return penggunaan;
    }

    public void setPenggunaan(PenggunaanModelsAuto penggunaan) {
        this.penggunaan = penggunaan;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
