package com.refillmybottle.refilmybottle.statistik.POJOSTATISTIK;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PenggunaanModelsAuto {
    @SerializedName("daily")
    private int daily;
    @SerializedName("total")
    private int total;
    @SerializedName("monthly")
    private List<MonthlyModelsAuto> monthly;

    public static PenggunaanModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, PenggunaanModelsAuto.class);
    }

    public int getDaily() {
        return daily;
    }

    public void setDaily(int daily) {
        this.daily = daily;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<MonthlyModelsAuto> getMonthly() {
        return monthly;
    }

    public void setMonthly(List<MonthlyModelsAuto> monthly) {
        this.monthly = monthly;
    }
}
