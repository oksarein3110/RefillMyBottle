package com.refillmybottle.refilmybottle.statistik;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DailyWater extends AppCompatActivity {

    @BindView(R.id.backDaily)
    ImageView backDaily;
    @BindView(R.id.titleDailyWater)
    TextView titleDailyWater;
    @BindView(R.id.imageBottleStat)
    ImageView imageBottleStat;
    @BindView(R.id.statusfullBottle)
    TextView statusfullBottle;
    @BindView(R.id.percentageBottle)
    TextView percentageBottle;
    @BindView(R.id.currentStatusBottle)
    TextView currentStatusBottle;
    @BindView(R.id.coordBottleStatus)
    ConstraintLayout coordBottleStatus;
    @BindView(R.id.intakeBottle)
    TextView intakeBottle;
    @BindView(R.id.frameStatusBottle)
    FrameLayout frameStatusBottle;

    SessionManager sessionManager;
    TextView valueBottle, submit;
    ImageView plus, minus;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_water);
        sessionManager = new SessionManager(this);
        ButterKnife.bind(this);
        Fragment fragment = FragmentStatusBottle.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.frameStatusBottle, fragment).commit();
        Calendar rightNow = Calendar.getInstance();
        String CurrentHour = String.valueOf(rightNow.getTime().getHours()) + ":" + String.valueOf(rightNow.getTime().getMinutes());

        if(CurrentHour.equals("0:00")){
            sessionManager.saveSessionStr(sessionManager.SESSION_CURRENT_STAT_BOTTLE, "0");
            sessionManager.saveSessionStr(sessionManager.SESSION_PERCENTAGE,"0");
        }

        statusfullBottle.setText(sessionManager.getSessionMaxbottle().toString());
        currentStatusBottle.setText(sessionManager.getSessionCurrentStatBottle().toString());
        percentageBottle.setText(sessionManager.getSessionPercentage().toString());







    }

    @OnClick({R.id.backDaily, R.id.intakeBottle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backDaily:
                finish();
                break;
            case R.id.intakeBottle:
                dialogIntake();
                break;
        }
    }

    public void dialogIntake(){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.intake_dialog);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        valueBottle = (TextView) dialog.findViewById(R.id.valueBottle);
        submit = (TextView) dialog.findViewById(R.id.submitBtRefil);
        plus = (ImageView) dialog.findViewById(R.id.add);
        minus = (ImageView) dialog.findViewById(R.id.min);



        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer stat = Integer.parseInt(valueBottle.getText().toString());
                stat++;
                valueBottle.setText(stat.toString());
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer stat1 = Integer.parseInt(valueBottle.getText().toString());
                stat1--;
                valueBottle.setText(stat1.toString());
            }
        });



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float current = Float.parseFloat(currentStatusBottle.getText().toString());
                Float maxi = Float.parseFloat(statusfullBottle.getText().toString());
                Float target = Float.parseFloat(valueBottle.getText().toString()) / 1000;

                if ((current - target)>0){
                    Float curr1 = current - target;
                    Float percent = (curr1 / maxi) * 100;
                    Integer percent1 = Math.round(percent);

                    double temp = Math.pow(10, 1);
                    double curR = (double) Math.round(curr1 * temp)/temp;
                    String cur3 = Double.toString(curR);

                    currentStatusBottle.setText(cur3);
                    percentageBottle.setText(percent1.toString());

                    sessionManager.saveSessionStr(sessionManager.SESSION_CURRENT_STAT_BOTTLE, cur3);
                    sessionManager.saveSessionStr(sessionManager.SESSION_PERCENTAGE, percent.toString());

                    dialog.dismiss();

                    TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), "You've added " + valueBottle.getText().toString() + "ml water in daily water", TSnackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.WHITE);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#00BF21"));
                    TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                } else {
                    dialog.dismiss();
                    TSnackbar snackbar1 = TSnackbar.make(findViewById(android.R.id.content), "Your water in your bottle are minimum! Please Refill First!", TSnackbar.LENGTH_LONG);
                    snackbar1.setActionTextColor(Color.BLACK);
                    View snackbarView = snackbar1.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#EE020F"));
                    TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar1.show();

                }


            }
        });
        dialog.show();
    }
}
