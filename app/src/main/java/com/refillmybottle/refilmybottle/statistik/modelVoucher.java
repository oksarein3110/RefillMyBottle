package com.refillmybottle.refilmybottle.statistik;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abah on 21/08/18.
 */

public class modelVoucher {

    @SerializedName("id") private String id;
    @SerializedName("nama") private String nama;
    @SerializedName("poin") private String poin;
    @SerializedName("foto") private String foto;
    @SerializedName("kode") private String kode;

    public String getPoin() {
        return poin;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getFoto() {
        return foto;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public void setPoin(String poin) {
        this.poin = poin;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setId(String id) {
        this.id = id;
    }
}
