package com.refillmybottle.refilmybottle.statistik;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.statistik.POJOSTATISTIK.ResponseChart;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentEarnedPoints extends Fragment {

    @BindView(R.id.savedvalues)
    TextView savedvalues;
    Unbinder unbinder;
    SessionManager sessionManager;
    RequestInterfaces mReq;

    public FragmentEarnedPoints() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment_earned_points, container, false);
        sessionManager = new SessionManager(getContext());
        unbinder = ButterKnife.bind(this, view);
        mReq = Utils.getApiServices();

        mReq.getChartvalues(sessionManager.getSessionToken().toString()).enqueue(new Callback<ResponseChart>() {
            @Override
            public void onResponse(Call<ResponseChart> call, Response<ResponseChart> response) {
                savedvalues.setText(String.valueOf(response.body().getPenggunaan().getTotal()));
            }

            @Override
            public void onFailure(Call<ResponseChart> call, Throwable throwable) {

            }
        });

        savedvalues.setText(sessionManager.getSessionPoin().toString());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
