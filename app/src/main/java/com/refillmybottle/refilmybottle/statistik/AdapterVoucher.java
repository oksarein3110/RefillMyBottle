package com.refillmybottle.refilmybottle.statistik;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.Shop.AdapterPopularShop;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterVoucher extends RecyclerView.Adapter<AdapterVoucher.ViewHolder> {

    private Context context;
    private ArrayList<modelVoucher> data;
    RequestInterfaces mReq;
    SessionManager sessionManager;

    public AdapterVoucher(ArrayList<modelVoucher> data){
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterVoucher.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_voucher, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder( final AdapterVoucher.ViewHolder holder, final int position) {
        final String kode_voucher = data.get(position).getKode().toString();
        final String image_voucher = data.get(position).getFoto().toString();
        holder.name_voucher.setText(data.get(position).getNama().toString());
        holder.point.setText(data.get(position).getPoin().toString());
        Picasso.with(context)
                .load(image_voucher)
                .into(holder.image_voucher);
        holder.redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mReq.RedeemVoucher(sessionManager.getSessionToken().toString(), kode_voucher).enqueue(new Callback<ResponseRedeem>() {
                    @Override
                    public void onResponse(Call<ResponseRedeem> call, Response<ResponseRedeem> response) {
                    if(response.isSuccessful()){
                        if(response.body().getStatus().equals(200)){
                            Toast.makeText(view.getContext(), response.body().getMsg().toString(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(view.getContext(), response.body().getMsg().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    }

                    @Override
                    public void onFailure(Call<ResponseRedeem> call, Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name_voucher, point, redeem;
        private ImageView image_voucher;
        public ViewHolder(View view){
            super(view);
            context = view.getContext();
            sessionManager = new SessionManager(context);
            mReq = Utils.getApiServices();

            name_voucher = view.findViewById(R.id.title_voucher);
            point = view.findViewById(R.id.pointItem);
            image_voucher = view.findViewById(R.id.imageVoucher);
            redeem = view.findViewById(R.id.redeemVoucher);
        }

    }
}
