package com.refillmybottle.refilmybottle.statistik;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FragmentStatusBottle extends Fragment {


    CardView cardViewStatus20;
    CardView cardViewStatus40;


    public static FragmentStatusBottle newInstance() {
        FragmentStatusBottle fragmentStatusBottle = new FragmentStatusBottle();
        return fragmentStatusBottle;
    }

    SessionManager sessionManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment_status_bottle, container, false);
        sessionManager = new SessionManager(view.getContext());

        cardViewStatus20 = view.findViewById(R.id.cardViewStatus20);
        cardViewStatus40 = view.findViewById(R.id.cardViewStatus40);

        if(Float.parseFloat(sessionManager.getSessionPercentage()) >= 40.0){
            cardViewStatus40.setVisibility(View.VISIBLE);
        } else if (Float.parseFloat(sessionManager.getSessionPercentage()) >=20.0) {
            cardViewStatus20.setVisibility(View.VISIBLE);
            cardViewStatus40.setVisibility(View.VISIBLE);
        }



        return view;
    }


}
