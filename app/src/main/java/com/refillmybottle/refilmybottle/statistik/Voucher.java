package com.refillmybottle.refilmybottle.statistik;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.Event.CustomAdapter;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Voucher extends AppCompatActivity {

    @BindView(R.id.backbuttonvoucher)
    ImageView backbuttonvoucher;
    @BindView(R.id.recyclerVoucher)
    RecyclerView recyclerVoucher;
    RequestInterfaces mRequest;
    @BindView(R.id.myVoucherGo)
    TextView myVoucherGo;


    SessionManager sessionManager;
    private ArrayList<modelVoucher> data_voucher;
    private AdapterVoucher adapterVoucher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        mRequest = Utils.getApiServices();
        initView();
    }

    private void initView() {
        recyclerVoucher.setHasFixedSize(true);
        recyclerVoucher.setLayoutManager(new LinearLayoutManager(this));
        loadJSON();

    }

    private void loadJSON() {
        mRequest.getVoucher(sessionManager.getSessionToken().toString()).enqueue(new Callback<ResponseVoucher>() {
            @Override
            public void onResponse(Call<ResponseVoucher> call, Response<ResponseVoucher> response) {
                ResponseVoucher responseVoucher = response.body();
                data_voucher = new ArrayList<>(Arrays.asList(responseVoucher.getData()));
                adapterVoucher = new AdapterVoucher(data_voucher);
                recyclerVoucher.setAdapter(adapterVoucher);
            }

            @Override
            public void onFailure(Call<ResponseVoucher> call, Throwable t) {

            }
        });
    }


    @OnClick({R.id.backbuttonvoucher, R.id.myVoucherGo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backbuttonvoucher:
                startActivity(new Intent(Voucher.this, StatistikMainActivity.class));
                finish();
                break;
            case R.id.myVoucherGo:
                startActivity(new Intent(Voucher.this, MyVoucher.class));
                finish();
                break;
        }
    }
}
