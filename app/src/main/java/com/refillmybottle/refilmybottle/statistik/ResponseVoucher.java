package com.refillmybottle.refilmybottle.statistik;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abah on 21/08/18.
 */

public class ResponseVoucher {
    @SerializedName("status")
    private String status;
    @SerializedName("msg")
    private String msg;
    private modelVoucher[] data;
    public modelVoucher[] getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(modelVoucher[] data) {
        this.data = data;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
