package com.refillmybottle.refilmybottle.statistik.POJOSTATISTIK;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class MonthlyModelsAuto {
    @SerializedName("bulan")
    private String bulan;
    @SerializedName("botol_bln")
    private String botolBln;
    @SerializedName("max_botol")
    private String maxBotol;

    public static MonthlyModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, MonthlyModelsAuto.class);
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getBotolBln() {
        return botolBln;
    }

    public void setBotolBln(String botolBln) {
        this.botolBln = botolBln;
    }

    public String getMaxBotol() {
        return maxBotol;
    }

    public void setMaxBotol(String maxBotol) {
        this.maxBotol = maxBotol;
    }
}
