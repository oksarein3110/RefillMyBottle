package com.refillmybottle.refilmybottle.statistik;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ProgressReward extends Fragment {


    @BindView(R.id.memberlogo)
    ImageView memberlogo;
    @BindView(R.id.txtMember)
    TextView txtMember;
    @BindView(R.id.silverLogo)
    ImageView silverLogo;
    @BindView(R.id.logoSilver)
    TextView logoSilver;
    @BindView(R.id.logoGold)
    ImageView logoGold;
    @BindView(R.id.txtGold)
    TextView txtGold;
    @BindView(R.id.logoPlatinum)
    ImageView logoPlatinum;
    @BindView(R.id.txtPlatinum)
    TextView txtPlatinum;
    @BindView(R.id.LearnMoreRewards)
    TextView LearnMoreRewards;
    @BindView(R.id.progressTierBar)
    ProgressBar progressTierBar;

    SessionManager mSession;

    @BindView(R.id.sisaPoints)
    TextView sisaPoints;
    Unbinder unbinder;
    @BindView(R.id.statBar)
    TextView statBar;



    public static ProgressReward newInstance() {
        ProgressReward progressReward = new ProgressReward();
        return progressReward;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_progress_reward, container, false);
        unbinder = ButterKnife.bind(this, view);
        mSession = new SessionManager(getContext());
        initView();

        return view;
    }

    private void initView() {
        int value = Integer.parseInt(mSession.getSessionPoin().toString());
        progressTierBar.setMax(2000);
        progressTierBar.setProgress(value, true);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.statBar)
    public void onViewClicked() {
        ((StatistikMainActivity)getActivity()).changeStatistik();
    }
}
