package com.refillmybottle.refilmybottle.Event.POJODetailsEvent;

public class ModelDetailsEvent {

    /**
     * id : 2
     * tgl : 2018-08-21
     * nama : Amal Daging Sapi
     * keterangan : Beramal dengan Sapi
     * url : https://www.google.co.id/search?q=idul+adha
     * foto : http://localhost/htdocs/COMMIT/asset/evpic/default.png
     * by_user : admin@gmail.com
     * like : 1
     * share : 1
     */

    private String id;
    private String tgl;
    private String nama;
    private String keterangan;
    private String url;
    private String foto;
    private String by_user;
    private int like;
    private int share;
    private String alamat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getBy_user() {
        return by_user;
    }

    public void setBy_user(String by_user) {
        this.by_user = by_user;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getShare() {
        return share;
    }

    public void setShare(int share) {
        this.share = share;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
