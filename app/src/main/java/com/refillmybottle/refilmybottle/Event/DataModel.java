package com.refillmybottle.refilmybottle.Event;


public class DataModel {

    /**
     * id : 1
     * tgl : 2018-08-17
     * nama : Hari Kemerdekaan Indonesia
     * keterangan : Hari Kemerdekaan Indonesia 17 Agustus 2018 akan diadakan acara: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut lorem metus, facilisis eu nunc quis, congue luctus risus. Morbi consectetur, dolor at facilisis cursus, leo leo dictum nunc, id vehicula magna enim sed magna. Nulla orci sapien, faucibus eget turpis ut, rutrum eleifend lacus. Phasellus bibendum velit nisi, sit amet fermentum tortor venenatis eu. Nam lobortis dolor nulla, et bibendum nibh ultricies nec. Etiam posuere tempus elementum. Sed tempor elit sed augue vulputate rutrum accumsan sit amet arcu. Cras ullamcorper est interdum ligula aliquam condimentum. Quisque ut sapien condimentum tellus porta laoreet a et diam. Maecenas et nisl et tellus lacinia ornare in sed enim. Pellentesque luctus semper massa non sagittis. Aenean finibus sem quis lacus porttitor, ut vestibulum erat porta. Duis sodales tellus a pretium elementum.
     * url : https://www.google.co.id/search?q=hari+kemerdekaan+indonesia
     * foto : http://localhost/htdocs/COMMIT/asset/evpic/default.png
     * by_user : admin@gmail.com
     * like : 0
     * share : 0
     */

    private String id;
    private String tgl;
    private String nama;
    private String keterangan;
    private String url;
    private String foto;
    private String by_user;
    private int like;
    private int share;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getBy_user() {
        return by_user;
    }

    public void setBy_user(String by_user) {
        this.by_user = by_user;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getShare() {
        return share;
    }

    public void setShare(int share) {
        this.share = share;
    }
}
