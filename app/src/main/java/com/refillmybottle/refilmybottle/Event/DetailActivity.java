package com.refillmybottle.refilmybottle.Event;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.refillmybottle.refilmybottle.Event.POJODetailsEvent.ResponseDetailEvent;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {


    @BindView(R.id.imageDetailsEvent)
    ImageView imageDetailsEvent;
    @BindView(R.id.backButtonEvent)
    ImageView backButtonEvent;
    @BindView(R.id.headDetailMaps)
    CoordinatorLayout headDetailMaps;
    @BindView(R.id.titleDetailMaps)
    TextView titleDetailMaps;
    @BindView(R.id.datetimeEvent)
    TextView datetimeEvent;
    @BindView(R.id.love)
    ImageView love;
    @BindView(R.id.countHeart)
    TextView countHeart;
    @BindView(R.id.commentEventLogo)
    ImageView commentEventLogo;
    @BindView(R.id.commentCount)
    TextView commentCount;
    @BindView(R.id.shareLogo)
    ImageView shareLogo;
    @BindView(R.id.countShare)
    TextView countShare;
    @BindView(R.id.likeShare)
    LinearLayout likeShare;
    @BindView(R.id.contentEventDetails)
    TextView contentEventDetails;
    @BindView(R.id.material_design_floating_action_menu_item1)
    FloatingActionButton materialDesignFloatingActionMenuItem1;
    @BindView(R.id.material_design_floating_action_menu_item2)
    FloatingActionButton materialDesignFloatingActionMenuItem2;
    @BindView(R.id.fabPlusEvent)
    FloatingActionMenu fabPlusEvent;
    @BindView(R.id.containerDetailsEvent)
    CoordinatorLayout containerDetailsEvent;

    String url;
    int REQ_CODE = 1;

    RequestInterfaces mReq;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        sessionManager = new SessionManager(this);
        mReq = Utils.getApiServices();
        LoadData();
        ButterKnife.bind(this);

    }

    private void LoadData() {
        mReq.getEventMore(sessionManager.getSessionIdEvent(), sessionManager.getSessionToken()).enqueue(new Callback<ResponseDetailEvent>() {
            @Override
            public void onResponse(Call<ResponseDetailEvent> call, Response<ResponseDetailEvent> response) {
                ResponseDetailEvent responseDetailEvent = response.body();
                if(response.isSuccessful()){
                    for(int i=0; i < responseDetailEvent.getData().size(); i++){
                        String titleEvent = responseDetailEvent.getData().get(i).getNama();
                        String dateEvent = responseDetailEvent.getData().get(i).getTgl();
                        String address = responseDetailEvent.getData().get(i).getAlamat();
                        int likes = responseDetailEvent.getData().get(i).getLike();
                        int share = responseDetailEvent.getData().get(i).getShare();
                        String details = responseDetailEvent.getData().get(i).getKeterangan();
                        String images = responseDetailEvent.getData().get(i).getFoto();
                        url = responseDetailEvent.getData().get(i).getUrl();
                        Picasso.with(DetailActivity.this)
                                .load(images)
                                .into(imageDetailsEvent);
                        datetimeEvent.setText(dateEvent + " - " + address);
                        countHeart.setText(String.valueOf(likes));
                        countShare.setText(String.valueOf(share));
                        titleDetailMaps.setText(titleEvent);
                        contentEventDetails.setText(details);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseDetailEvent> call, Throwable t) {

            }
        });
    }


    @OnClick({R.id.backButtonEvent, R.id.material_design_floating_action_menu_item1, R.id.material_design_floating_action_menu_item2, R.id.fabPlusEvent})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButtonEvent:
                finish();
                break;
            case R.id.material_design_floating_action_menu_item1:
                LoveAction();
                break;
            case R.id.material_design_floating_action_menu_item2:
                shareProgr();
                break;
            case R.id.fabPlusEvent:

                break;
        }
    }

    private void LoveAction() {
        mReq.getLikesUnlikes(sessionManager.getSessionIdEvent(), "1", sessionManager.getSessionToken()).enqueue(new Callback<ResponseLikesUnlikes>() {
            @Override
            public void onResponse(Call<ResponseLikesUnlikes> call, Response<ResponseLikesUnlikes> response) {
                ResponseLikesUnlikes responseLikesUnlikes = response.body();
                if(response.isSuccessful()){
                    if(responseLikesUnlikes.getStatus().equals("200")){
                        Toast.makeText(DetailActivity.this, responseLikesUnlikes.getMsg(), Toast.LENGTH_SHORT).show();
                        LoadData();
                    } else {
                        Toast.makeText(DetailActivity.this, responseLikesUnlikes.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseLikesUnlikes> call, Throwable t) {

            }
        });
    }

    private void shareAction() {
        mReq.getLikesUnlikes(sessionManager.getSessionIdEvent(), "2", sessionManager.getSessionToken()).enqueue(new Callback<ResponseLikesUnlikes>() {
            @Override
            public void onResponse(Call<ResponseLikesUnlikes> call, Response<ResponseLikesUnlikes> response) {
                ResponseLikesUnlikes responseLikesUnlikes = response.body();
                if(response.isSuccessful()){
                    if(responseLikesUnlikes.getStatus().equals("200")){
                        Toast.makeText(DetailActivity.this, responseLikesUnlikes.getMsg(), Toast.LENGTH_SHORT).show();
                        LoadData();
                    } else {
                        Toast.makeText(DetailActivity.this, responseLikesUnlikes.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseLikesUnlikes> call, Throwable t) {

            }
        });
    }

    public void shareProgr(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "RefillMyBottle");
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivityForResult(Intent.createChooser(sendIntent.setType("text/plain"), "Share this Event Via ; "), REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == Activity.RESULT_OK){
            shareAction();
        } else if (resultCode == Activity.RESULT_CANCELED){
            Toast.makeText(DetailActivity.this, "Share Canceled!", Toast.LENGTH_SHORT).show();
        }
    }

}
