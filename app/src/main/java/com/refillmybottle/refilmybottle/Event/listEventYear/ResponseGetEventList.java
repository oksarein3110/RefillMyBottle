package com.refillmybottle.refilmybottle.Event.listEventYear;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetEventList {

    /**
     * status : 200
     * data : [{"id":"1","tgl":"2018-08-17","nama":"Hari Kemerdekaan Indonesia","alamat":"Jl. Saja Dulu","keterangan":"Hari Kemerdekaan Indonesia 17 Agustus 2018 akan diadakan acara: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut lorem metus, facilisis eu nunc quis, congue luctus risus. Morbi consectetur, dolor at facilisis cursus, leo leo dictum nunc, id vehicula magna enim sed magna. Nulla orci sapien, faucibus eget turpis ut, rutrum eleifend lacus. Phasellus bibendum velit nisi, sit amet fermentum tortor venenatis eu. Nam lobortis dolor nulla, et bibendum nibh ultricies nec. Etiam posuere tempus elementum. Sed tempor elit sed augue vulputate rutrum accumsan sit amet arcu. Cras ullamcorper est interdum ligula aliquam condimentum. Quisque ut sapien condimentum tellus porta laoreet a et diam. Maecenas et nisl et tellus lacinia ornare in sed enim. Pellentesque luctus semper massa non sagittis. Aenean finibus sem quis lacus porttitor, ut vestibulum erat porta. Duis sodales tellus a pretium elementum.","url":"https://www.google.co.id/search?q=hari+kemerdekaan+indonesia","foto":"http://192.168.1.20/htdocs/magang/asset/evpic/default.png","by_user":"admin@gmail.com","like":0,"share":1},{"id":"2","tgl":"2018-08-21","nama":"Amal Daging Sapi","alamat":"Jl. Kenangan","keterangan":"Beramal dengan Sapi","url":"https://www.google.co.id/search?q=idul+adha","foto":"http://192.168.1.20/htdocs/magang/asset/evpic/default.png","by_user":"admin@gmail.com","like":2,"share":2},{"id":"10","tgl":"2018-10-28","nama":"Sumpah Pemuda","alamat":"Jl. Jalan","keterangan":"Sumpah Pemuda 2018 @ Juanda - Jakarta","url":"https://id.wikipedia.org/wiki/Sumpah_Pemuda","foto":"http://192.168.1.20/htdocs/magang/asset/evpic/default.png","by_user":"admin@gmail.com","like":0,"share":0}]
     * msg : OK
     */

    @SerializedName("status")
    private String status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }
}
