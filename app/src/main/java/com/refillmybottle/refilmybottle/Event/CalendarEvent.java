package com.refillmybottle.refilmybottle.Event;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.refillmybottle.refilmybottle.Event.listEventYear.DataBean;
import com.refillmybottle.refilmybottle.Event.listEventYear.ResponseGetEventList;
import com.refillmybottle.refilmybottle.FragmentParent;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarEvent extends Fragment {

    SessionManager sessionManager;
    RequestInterfaces requestInterfaces;
    String tgl;
    ArrayList<DataModel> dataModels;
    CustomAdapter adapter;

    @BindView(R.id.handle)
    Button handle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.content)
    LinearLayout content;
    @BindView(R.id.simpleSlidingDrawer)
    SlidingDrawer simpleSlidingDrawer;
    @BindView(R.id.txtNotfoundr)
    TextView txtNotfoundr;
    Unbinder unbinder;
    @BindView(R.id.compactcalendar_view)
    CompactCalendarView compactcalendarView;

    private Toolbar toolbar;
    private ActionBar actionBar;
    private SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MM");
    private SimpleDateFormat yearsFormat = new SimpleDateFormat("yyyy");
    private TextView txtMonth, txtYears;

    ArrayAdapter<String> adapters;
    public static CalendarEvent newInstance() {
        CalendarEvent calendarEvent = new CalendarEvent();
        return calendarEvent;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_calendar_event, container, false);
        sessionManager = new SessionManager(getContext());
        requestInterfaces = Utils.getApiServices();
        unbinder = ButterKnife.bind(this, view);

        toolbar = (Toolbar) view.findViewById(R.id.tool_bar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(null);
        compactcalendarView = (CompactCalendarView) view.findViewById(R.id.compactcalendar_view);
        compactcalendarView.setUseThreeLetterAbbreviation(true);
        compactcalendarView.setUseThreeLetterAbbreviation(true);
        txtMonth = view.findViewById(R.id.txtMonth);
        txtYears = view.findViewById(R.id.txtYears);






        txtMonth.setText(getMonth(Integer.parseInt(dateFormatMonth.format(compactcalendarView.getFirstDayOfCurrentMonth()))));
        txtYears.setText(yearsFormat.format(compactcalendarView.getFirstDayOfCurrentMonth()));




        initViews();
        getToday();
        return view;
    }

    private void getToday() {
        Date date = new Date();
        String now = String.valueOf(date.getYear()+1900) + "-" + String.valueOf(date.getMonth()+1) + "-" + String.valueOf(date.getDate());
        requestInterfaces.getDetailEvent(sessionManager.getSessionToken().toString(), now).enqueue(new Callback<ResponEventKlik>() {
            @Override
            public void onResponse(Call<ResponEventKlik> call, Response<ResponEventKlik> response) {
                if(response.isSuccessful()){
                    if(response.body().getMsg().equals("NO DATA")){
                        txtNotfoundr.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        txtNotfoundr.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        dataModels = new ArrayList<>(Arrays.asList(response.body().getData()));
                        adapter = new CustomAdapter(dataModels);
                        recyclerView.setAdapter(adapter);
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponEventKlik> call, Throwable t) {

            }
        });
    }



    private void initViews() {
        addEvent();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);


        compactcalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                tgl = String.valueOf(dateClicked.getYear() + 1900) + "-" + String.valueOf(dateClicked.getMonth() + 1) + "-" + String.valueOf(dateClicked.getDate());
                getEvent();
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                txtMonth.setText(getMonth(Integer.parseInt(dateFormatMonth.format(firstDayOfNewMonth))));
                txtYears.setText(yearsFormat.format(firstDayOfNewMonth));
            }
        });




    }

    private void getEvent() {
        requestInterfaces.getDetailEvent(sessionManager.getSessionToken().toString(), tgl).enqueue(new Callback<ResponEventKlik>() {
            @Override
            public void onResponse(Call<ResponEventKlik> call, Response<ResponEventKlik> response) {
                if(response.isSuccessful()){
                    if(response.body().getMsg().equals("NO DATA")){
                        txtNotfoundr.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        txtNotfoundr.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        dataModels = new ArrayList<>(Arrays.asList(response.body().getData()));
                        adapter = new CustomAdapter(dataModels);
                        recyclerView.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponEventKlik> call, Throwable t) {

            }
        });
    }


    public void addEvent(){
        requestInterfaces.getEventLists().enqueue(new Callback<ResponseGetEventList>() {
            @Override
            public void onResponse(Call<ResponseGetEventList> call, Response<ResponseGetEventList> response) {
                ResponseGetEventList responseGetEventList = response.body();
                for(int i=0; i< responseGetEventList.getData().size(); i++){
                    String date = responseGetEventList.getData().get(i).getTgl();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date date1 = (Date)formatter.parse(date);
                        String time = String.valueOf(date1.getTime());
                        Event event = new Event(Color.WHITE, Long.parseLong(time));
                        compactcalendarView.addEvent(event);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseGetEventList> call, Throwable t) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }


}
