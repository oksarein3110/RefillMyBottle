package com.refillmybottle.refilmybottle.Event.listEventYear;

import com.google.gson.annotations.SerializedName;

public class DataBean {
    /**
     * id : 1
     * tgl : 2018-08-17
     * nama : Hari Kemerdekaan Indonesia
     * alamat : Jl. Saja Dulu
     * keterangan : Hari Kemerdekaan Indonesia 17 Agustus 2018 akan diadakan acara: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut lorem metus, facilisis eu nunc quis, congue luctus risus. Morbi consectetur, dolor at facilisis cursus, leo leo dictum nunc, id vehicula magna enim sed magna. Nulla orci sapien, faucibus eget turpis ut, rutrum eleifend lacus. Phasellus bibendum velit nisi, sit amet fermentum tortor venenatis eu. Nam lobortis dolor nulla, et bibendum nibh ultricies nec. Etiam posuere tempus elementum. Sed tempor elit sed augue vulputate rutrum accumsan sit amet arcu. Cras ullamcorper est interdum ligula aliquam condimentum. Quisque ut sapien condimentum tellus porta laoreet a et diam. Maecenas et nisl et tellus lacinia ornare in sed enim. Pellentesque luctus semper massa non sagittis. Aenean finibus sem quis lacus porttitor, ut vestibulum erat porta. Duis sodales tellus a pretium elementum.
     * url : https://www.google.co.id/search?q=hari+kemerdekaan+indonesia
     * foto : http://192.168.1.20/htdocs/magang/asset/evpic/default.png
     * by_user : admin@gmail.com
     * like : 0
     * share : 1
     */

    @SerializedName("id")
    private String id;
    @SerializedName("tgl")
    private String tgl;
    @SerializedName("nama")
    private String nama;
    @SerializedName("alamat")
    private String alamat;
    @SerializedName("keterangan")
    private String keterangan;
    @SerializedName("url")
    private String url;
    @SerializedName("foto")
    private String foto;
    @SerializedName("by_user")
    private String byUser;
    @SerializedName("like")
    private int like;
    @SerializedName("share")
    private int share;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getByUser() {
        return byUser;
    }

    public void setByUser(String byUser) {
        this.byUser = byUser;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getShare() {
        return share;
    }

    public void setShare(int share) {
        this.share = share;
    }
}
