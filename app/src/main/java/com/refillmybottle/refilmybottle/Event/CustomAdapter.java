package com.refillmybottle.refilmybottle.Event;


import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private ArrayList<DataModel> data;
    Context context;
    SessionManager sessionManager;

    public CustomAdapter(ArrayList<DataModel> data){
        this.data = data;
    }

    @NonNull
    @Override
    public CustomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomAdapter.MyViewHolder holder, final int position) {
        String images = data.get(position).getFoto().toString();
        holder.judul.setText(data.get(position).getNama());
        holder.tanggal.setText(data.get(position).getTgl());
        Picasso.with(context).load(images).into(holder.fotoEvent);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailActivity.class);
                sessionManager.saveSessionStr(sessionManager.SESSION_ID_EVENT, data.get(position).getId().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView judul, tanggal;
        private ImageView fotoEvent;
        private CardView cardView;
        public MyViewHolder(View view){
            super (view);
            context = view.getContext();
            sessionManager = new SessionManager(view.getContext());
            judul = view.findViewById(R.id.judul_event);
            tanggal = view.findViewById(R.id.date_event);
            fotoEvent = view.findViewById(R.id.daftar_icon);
            cardView = view.findViewById(R.id.card_viewEvent);
        }
    }

}
