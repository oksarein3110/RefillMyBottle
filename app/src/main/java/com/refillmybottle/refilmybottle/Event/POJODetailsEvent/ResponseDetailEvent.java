package com.refillmybottle.refilmybottle.Event.POJODetailsEvent;

import com.google.gson.annotations.SerializedName;
import com.refillmybottle.refilmybottle.Maps.POJODetailMaps.DataModels;

import java.util.List;

public class ResponseDetailEvent {
    @SerializedName("status")
    private String status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private List<ModelDetailsEvent> data;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ModelDetailsEvent> getData() {
        return data;
    }

    public void setData(List<ModelDetailsEvent> data) {
        this.data = data;
    }
}
