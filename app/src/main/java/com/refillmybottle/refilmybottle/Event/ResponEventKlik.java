package com.refillmybottle.refilmybottle.Event;

import com.google.gson.annotations.SerializedName;


import java.util.List;

public class ResponEventKlik {
   private DataModel[] data;
    @SerializedName("msg")
    private String msg;
    public DataModel[] getData() {
        return data;
    }

    public void setData(DataModel[] data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
