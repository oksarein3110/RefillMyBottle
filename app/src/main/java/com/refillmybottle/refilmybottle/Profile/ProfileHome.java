package com.refillmybottle.refilmybottle.Profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.Profile;
import com.facebook.internal.ImageRequest;
import com.facebook.login.LoginManager;
import com.refillmybottle.refilmybottle.About;
import com.refillmybottle.refilmybottle.Login;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.response.ResponseRefresh;
import com.refillmybottle.refilmybottle.statistik.StatistikMainActivity;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileHome extends Fragment {
    @BindView(R.id.profile_foto)
    CircleImageView profileFoto;
    @BindView(R.id.profile_name)
    TextView profileName;
    @BindView(R.id.city_profile)
    TextView cityProfile;
    @BindView(R.id.change_profile_name)
    CircleImageView changeProfileName;
    @BindView(R.id.intro_bio_profile)
    TextView introbioProfile;
    @BindView(R.id.account_settings)
    TextView accountSettings;
    @BindView(R.id.statistic)
    TextView statistic;
    @BindView(R.id.points_profile)
    TextView pointsProfile;
    @BindView(R.id.share_with_friend)
    TextView shareWithFriend;
    @BindView(R.id.about_us)
    TextView aboutUs;
    @BindView(R.id.terms_of_service)
    TextView termsOfService;
    @BindView(R.id.logout_but)
    TextView logout_buT;
    Unbinder unbinder;
    SessionManager sessionManager;
    RequestInterfaces mRequestInterfaces;
    @BindView(R.id.notifRed)
    ImageView notifRed;

    public static ProfileHome newInstance() {
        ProfileHome profileHome = new ProfileHome();
        return profileHome;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        sessionManager = new SessionManager(getContext());
        mRequestInterfaces = Utils.getApiServices();

        getData();


        if(sessionManager.getSessionFb().equals("1")){

            int dimensionPixelSize = getResources().getDimensionPixelSize(com.facebook.R.dimen.com_facebook_profilepictureview_preset_size_large);
            Uri profilePictureUri= Profile.getCurrentProfile().getProfilePictureUri(dimensionPixelSize , dimensionPixelSize);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
            getData();
            Glide.with(getContext()).load(profilePictureUri)
                    .apply(requestOptions)
                    .into(profileFoto);
            profileName.setText(Profile.getCurrentProfile().getName());
            pointsProfile.setText(sessionManager.getSessionPoin());

        } else {
            Picasso.with(getContext())
                    .load(sessionManager.getSessionPhoto().toString())
                    .into(profileFoto);
            getData();
            pointsProfile.setText(sessionManager.getSessionPoin().toString() + " pts");
            profileName.setText(sessionManager.getSessionNmdp().toString() + " " + sessionManager.getSessionNmbk().toString());
            cityProfile.setText(sessionManager.getSessionCity().toString() + " , " + sessionManager.getSessionRegion());
        }



//Dipindah ke get Data karena bentrok dengan facebook API nanti di handling kondisi


        return view;


    }

    private void getData() {
        mRequestInterfaces.getRefreshProfile(sessionManager.getSessionToken().toString()).enqueue(new Callback<ResponseRefresh>() {
            @Override
            public void onResponse(Call<ResponseRefresh> call, Response<ResponseRefresh> response) {
                if(response.isSuccessful()){
                    ResponseRefresh responseRefresh = response.body();
                    String poin = responseRefresh.getUser().getPoin();
                    String namedp = responseRefresh.getUser().getNamadpn();
                    String nmbk = responseRefresh.getUser().getNamablk();
                    String foto = responseRefresh.getUser().getPhoto();
                    String city = responseRefresh.getUser().getKota();
                    String state = responseRefresh.getUser().getProv();
                    String region = responseRefresh.getUser().getNegara();
                    sessionManager.saveSessionStr(sessionManager.SESSION_POIN, poin);
                    sessionManager.saveSessionStr(sessionManager.SESSION_NMDP, namedp);
                    sessionManager.saveSessionStr(sessionManager.SESSION_NMBK, nmbk);
                    sessionManager.saveSessionStr(sessionManager.SESSION_PHOTO, foto);
                    sessionManager.saveSessionStr(sessionManager.SESSION_CITY, city);
                    sessionManager.saveSessionStr(sessionManager.SESSION_STATE, state);
                    sessionManager.saveSessionStr(sessionManager.SESSION_COUNTRY, region);
                }

            }

            @Override
            public void onFailure(Call<ResponseRefresh> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public static Bitmap getFacebookProfilePicture(String userID){
        URL imageURL;
        Bitmap bitmap=null;
        try {
            imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }

        return bitmap;
    }

    @OnClick({R.id.change_profile_name,R.id.account_settings, R.id.statistic, R.id.points_profile, R.id.share_with_friend, R.id.about_us, R.id.terms_of_service, R.id.logout_but})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.account_settings:
                startActivity(new Intent(getActivity().getApplication(), AccountSetting.class));
                break;
            case R.id.statistic:
                Intent intent = new Intent(getActivity().getApplication(), StatistikMainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("poin", sessionManager.getSessionPoin().toString());
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.points_profile:
                break;
            case R.id.share_with_friend:
                startActivity(new Intent(getActivity().getApplication(), Share.class));
                break;
            case R.id.about_us:
                startActivity(new Intent(getActivity().getApplication(), About.class));
                break;
            case R.id.terms_of_service:
                break;
            case R.id.logout_but:
                    sessionLogout();
                break;
            case R.id.change_profile_name:
                startActivity(new Intent(getActivity().getApplication(), EditProfile.class));
                break;
        }
    }

    public void sessionLogout() {
        String token = sessionManager.getSessionToken().toString();


        mRequestInterfaces.getLogout(token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    sessionManager.saveSessionBoolean(sessionManager.SESSION_STATUS, false);
                    sessionManager.saveSessionStr(sessionManager.SESSION_ID_FB, "");
                    sessionManager.saveSessionBoolean(sessionManager.SESSION_FB, false);
                    LoginManager.getInstance().logOut();
                    Toast.makeText(getActivity().getApplication(), "Success Logout !", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity().getApplication(), Login.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    getActivity().finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
