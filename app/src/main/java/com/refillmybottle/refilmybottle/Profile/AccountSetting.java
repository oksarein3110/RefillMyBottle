package com.refillmybottle.refilmybottle.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountSetting extends AppCompatActivity {

    @BindView(R.id.back_button_ep)
    ImageView backButtonEp;
    @BindView(R.id.edit_profile)
    TextView editProfile;
    @BindView(R.id.change_email_addr)
    TextView changeEmailAddr;
    @BindView(R.id.change_pass)
    TextView changePass;
    @BindView(R.id.update_interest)
    TextView updateInterest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setting);
        ButterKnife.bind(this);
        backButtonEp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @OnClick({R.id.edit_profile, R.id.change_email_addr, R.id.change_pass, R.id.update_interest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_profile:
                startActivity(new Intent(AccountSetting.this, EditProfile.class));
                break;
            case R.id.change_email_addr:
                startActivity(new Intent(AccountSetting.this, EditEmail.class));
                break;
            case R.id.change_pass:
                startActivity(new Intent(AccountSetting.this, EditPassword.class));
                break;
            case R.id.update_interest:
                startActivity(new Intent(AccountSetting.this, InterestIn.class));
                break;
        }
    }
}
