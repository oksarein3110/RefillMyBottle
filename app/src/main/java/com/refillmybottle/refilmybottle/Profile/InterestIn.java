package com.refillmybottle.refilmybottle.Profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


import com.refillmybottle.refilmybottle.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InterestIn extends AppCompatActivity {


    @BindView(R.id.backButtonInterest)
    ImageView backButtonInterest;
    @BindView(R.id.recyclerSelectedInter)
    RecyclerView recyclerSelectedInter;
    @BindView(R.id.recyclerListInterest)
    RecyclerView recyclerListInterest;
    @BindView(R.id.saveInterest)
    Button saveInterest;

    String[] interest = {"coffee", "cowork", "tea", "lorem", "ipsum", "cake", "prize"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_in);
        ButterKnife.bind(this);
        initComponents();

    }

    private void initComponents() {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
        recyclerListInterest.setLayoutManager(staggeredGridLayoutManager);
        AdapterInterest adapterInterest = new AdapterInterest(this, interest);
        recyclerListInterest.setAdapter(adapterInterest);
    }

    @OnClick({R.id.backButtonInterest, R.id.saveInterest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backButtonInterest:
                finish();
                break;
            case R.id.saveInterest:

                break;
        }
    }
}
