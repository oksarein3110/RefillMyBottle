package com.refillmybottle.refilmybottle.Profile;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;

public class AdapterInterest extends RecyclerView.Adapter<AdapterInterest.ViewHolder>{
   Context context;
   String[] interest;
   public AdapterInterest(Context context, String[] interest){
       this.context = context;
       this.interest = interest;
   }

    @NonNull
    @Override
    public AdapterInterest.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rows_interest, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterInterest.ViewHolder holder, int position) {
        holder.interst.setText(interest[position]);
        holder.interst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return interest.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView interst;
        public ViewHolder(View itemView) {
            super(itemView);
            interst = itemView.findViewById(R.id.txtInteres);
        }
    }
}
