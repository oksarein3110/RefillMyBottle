package com.refillmybottle.refilmybottle.Profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Share extends AppCompatActivity {

    @BindView(R.id.backShare)
    ImageView backShare;
    @BindView(R.id.titleShares)
    TextView titleShares;
    @BindView(R.id.descriptShare)
    TextView descriptShare;
    @BindView(R.id.txtFacebook)
    TextView txtFacebook;
    @BindView(R.id.statusFacebook)
    TextView statusFacebook;
    @BindView(R.id.linearfb)
    LinearLayout linearfb;
    @BindView(R.id.twitter)
    TextView twitter;
    @BindView(R.id.statusTwitter)
    TextView statusTwitter;
    @BindView(R.id.lineartwit)
    LinearLayout lineartwit;
    @BindView(R.id.instagram)
    TextView instagram;
    @BindView(R.id.statusInstagram)
    TextView statusInstagram;
    @BindView(R.id.linearIg)
    LinearLayout linearIg;
    @BindView(R.id.googlePlus)
    TextView googlePlus;
    @BindView(R.id.statusGooglePlus)
    TextView statusGooglePlus;
    @BindView(R.id.lineargooglePlus)
    LinearLayout lineargooglePlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.backShare, R.id.statusFacebook, R.id.statusTwitter, R.id.statusInstagram, R.id.statusGooglePlus})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backShare:
                finish();
                break;
            case R.id.statusFacebook:
                break;
            case R.id.statusTwitter:
                break;
            case R.id.statusInstagram:
                break;
            case R.id.statusGooglePlus:
                break;
        }
    }
}
