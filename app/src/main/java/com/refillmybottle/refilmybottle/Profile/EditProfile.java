package com.refillmybottle.refilmybottle.Profile;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.response.ItemCity;
import com.refillmybottle.refilmybottle.response.ItemCountry;
import com.refillmybottle.refilmybottle.response.ItemState;
import com.refillmybottle.refilmybottle.response.ResponseCity;
import com.refillmybottle.refilmybottle.response.ResponseCountry;
import com.refillmybottle.refilmybottle.response.response_state;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile extends AppCompatActivity {

    @BindView(R.id.back_arrows)
    ImageView backArrows;
    @BindView(R.id.profile_fotos)
    CircleImageView profileFotos;
    @BindView(R.id.edit_profiles_photo)
    ImageView editProfilesPhoto;
    @BindView(R.id.label_fullname)
    TextView labelFullname;
    @BindView(R.id.et_fullname)
    EditText etFullname;
    @BindView(R.id.label_dob)
    TextView labelDob;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.et_country_reg)
    Spinner etCountryReg;
    @BindView(R.id.label_state)
    TextView labelState;
    @BindView(R.id.et_state)
    Spinner etState;
    @BindView(R.id.label_city)
    TextView labelCity;
    @BindView(R.id.et_city)
    Spinner etCity;
    @BindView(R.id.label_street)
    TextView labelStreet;
    @BindView(R.id.et_street)
    EditText etStreet;
    @BindView(R.id.bt_save)
    Button btSave;
    int mYear, mMonth, mDay;
    String Country, State, City, token;

    RequestInterfaces mRequest;
    SessionManager sessionManager;
    Context mContext;

    private Uri outputFileUri;
    private static int TAKE_PICTURE = 1;
    public String images;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        mContext = this;
        mRequest = Utils.getApiServices();
        initComponent();
    }

    public void initComponent() {
        String Nama = sessionManager.getSessionNmdp().toString() + sessionManager.getSessionNmbk().toString();
        String DoB = sessionManager.getSessionDob().toString();
        Country = sessionManager.getSessionCountry().toString();
        State = sessionManager.getSessionRegion().toString();
        City = sessionManager.getSessionCity().toString();
        String Street = sessionManager.getSessionStreet().toString();
        token = sessionManager.getSessionToken().toString();
        etFullname.setText(Nama);
        etStreet.setText(Street);
        etDob.setText(DoB);

        Picasso.with(this).load(sessionManager.getSessionPhoto().toString()).into(profileFotos);


        getCountry();
        etCountryReg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getState();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        etState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getCity();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        etCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void getCountry() {
        mRequest.getCountry().enqueue(new Callback<ResponseCountry>() {
            @Override
            public void onResponse(Call<ResponseCountry> call, Response<ResponseCountry> response) {
                if (response.isSuccessful()) {
                    List<ItemCountry> responseCountries = response.body().getData();
                    List<String> listCountry = new ArrayList<String>();
                    for (int i = 0; i < responseCountries.size(); i++) {
                        listCountry.add(responseCountries.get(i).getCountry());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, listCountry);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    etCountryReg.setAdapter(adapter);
                    int spinnerPos = adapter.getPosition(Country);
                    etCountryReg.setSelection(spinnerPos);
                }
            }

            @Override
            public void onFailure(Call<ResponseCountry> call, Throwable t) {

            }
        });
    }

    public void getState() {

        mRequest.getState(etCountryReg.getSelectedItem().toString()).enqueue(new Callback<response_state>() {
            @Override
            public void onResponse(Call<response_state> call, Response<response_state> response) {
                if(response.isSuccessful()){
                    List<ItemState> responseState = response.body().getData();
                    List<String> listState = new ArrayList<String>();
                    for (int i=0; i < responseState.size(); i++){
                        listState.add(responseState.get(i).getState());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, listState);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    etState.setAdapter(adapter);
                    int statePost = adapter.getPosition(State);
                    etState.setSelection(statePost);
                }
            }

            @Override
            public void onFailure(Call<response_state> call, Throwable t) {

            }
        });
    }

    public void getCity() {
        mRequest.getCity(etState.getSelectedItem().toString()).enqueue(new Callback<ResponseCity>() {
            @Override
            public void onResponse(Call<ResponseCity> call, Response<ResponseCity> response) {
                if (response.isSuccessful()){
                    List<ItemCity> responseCity = response.body().getData();
                    List<String> listCity = new ArrayList<String>();
                    for(int i=0; i< responseCity.size(); i++){
                        listCity.add(responseCity.get(i).getCity());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, listCity);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    etCity.setAdapter(adapter);
                    int PostCity = adapter.getPosition(City);
                    etCity.setSelection(PostCity);
                }
            }

            @Override
            public void onFailure(Call<ResponseCity> call, Throwable t) {

            }
        });

    }

    @OnClick({R.id.back_arrows, R.id.edit_profiles_photo, R.id.bt_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_arrows:
                finish();
                break;
            case R.id.edit_profiles_photo:
                TakePhoto();
                break;
            case R.id.bt_save:
                progressUpdate();
                break;
        }
    }

    private void progressUpdate() {
    mRequest.getUpdateProfile(token, etFullname.getText().toString(), etDob.getText().toString(), etCountryReg.getSelectedItem().toString(), etState.getSelectedItem().toString(),
            etCity.getSelectedItem().toString(), etStreet.getText().toString(), images).enqueue(new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            Toast.makeText(EditProfile.this, "Data Changes Successfully!", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable throwable) {

        }
    });

    }

    private void TakePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(intent, TAKE_PICTURE);

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TAKE_PICTURE) {
            if (resultCode == 0) {
                return;
            }
            // photo taken


            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), outputFileUri);
            } catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }//BitmapFactory.decodeFile(tmpFile.getAbsolutePath());
            Matrix matrix = new Matrix();

            // resize the bit map
            float scale = (float) profileFotos.getMeasuredWidth() / bitmap.getWidth();
            matrix.postScale(scale, scale);

            // recreate the new Bitmap\
            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);

            profileFotos.setImageBitmap(resizedBitmap);

            images = encodeFromString(bitmap);
            sessionManager.saveSessionStr(sessionManager.SESSION_UPLOAD_IMAGE_REG, images);

        }
    }

    public static String encodeFromString(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 40, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    private void datePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                etDob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    @OnClick(R.id.et_dob)
    public void onViewClicked() {
        datePicker();
    }

    @Override
    protected void onResume() {
        super.onResume();
        etState.setSelection(0);
        etCity.setSelection(0);
        etCity.setSelection(0);
    }
}
