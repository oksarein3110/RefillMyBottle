package com.refillmybottle.refilmybottle.Profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditEmail extends AppCompatActivity {

    @BindView(R.id.back_arrows)
    ImageView backArrows;
    @BindView(R.id.et_currentmail)
    EditText etCurrentmail;
    @BindView(R.id.et_newemail)
    EditText etNewemail;
    @BindView(R.id.et_confirmemail)
    EditText etConfirmemail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.saves)
    Button saves;
    SessionManager sessionManager;
    Context mContext;
    RequestInterfaces mReq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_email);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        mContext = this;
        mReq = Utils.getApiServices();
        initComponent();
    }

   public void initComponent(){
        String Current = sessionManager.getSessionEmail().toString();
        etCurrentmail.setText(Current);
    }

    @OnClick({R.id.back_arrows, R.id.saves})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_arrows:
                finish();
                break;
            case R.id.saves:
                saveProcess();
                break;
        }
    }

    private void saveProcess() {
    mReq.getEditmail(etCurrentmail.getText().toString(), etNewemail.getText().toString(), etConfirmemail.getText().toString(), etPassword.getText().toString()).enqueue(new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if(response.isSuccessful()){
                try{
                    JSONObject jsonResult = new JSONObject(response.body().string());
                    sessionManager.saveSessionStr(sessionManager.SESSION_EMAIL, etCurrentmail.getText().toString());
                    String msg = jsonResult.getString("msg").toString();
                    Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();

                } catch (JSONException e){
                    e.printStackTrace();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {

        }
    });
    }
}
