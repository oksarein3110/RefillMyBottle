package com.refillmybottle.refilmybottle;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by abah on 31/07/18.
 */

public class CustomDialog extends Dialog {
    public CustomDialog(@NonNull Context context, int resLayout, DialogListener listener) {
        super(context);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(resLayout);
        listener.onInit(this);
        setCancelable(false);

    }

    public interface DialogListener{
        void onInit(Dialog dialog);
    }
}