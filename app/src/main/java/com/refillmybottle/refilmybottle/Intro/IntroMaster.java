package com.refillmybottle.refilmybottle.Intro;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rd.PageIndicatorView;
import com.refillmybottle.refilmybottle.Login;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.statistik.FragmentBottleSaved;
import com.refillmybottle.refilmybottle.statistik.FragmentEarnedPoints;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IntroMaster extends AppCompatActivity {

    @BindView(R.id.skip_button)
    TextView skipButton;
    @BindView(R.id.pagerViewIntro)
    ViewPager pagerViewIntro;
    @BindView(R.id.pagerIndicator)
    PageIndicatorView pagerIndicator;
    @BindView(R.id.previousIntro)
    ImageView previousIntro;
    @BindView(R.id.nextIntro)
    ImageView nextIntro;
    SessionManager sessionManager;

   final int PERMISSION_READ_EXTERNAL = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_master);
        ButterKnife.bind(this);

        cekIjin();

        sessionManager = new SessionManager(this);
        pagerIndicator.setCount(5);
        pagerIndicator.setSelection(5);

        SimpleFragmentAdapter simpleFragmentAdapter = new SimpleFragmentAdapter(this, getSupportFragmentManager());
        pagerViewIntro.setAdapter(simpleFragmentAdapter);

        pagerViewIntro.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pagerIndicator.setSelection(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void cekIjin() {
        if (ContextCompat.checkSelfPermission(IntroMaster.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(IntroMaster.this, Manifest.permission.READ_EXTERNAL_STORAGE)){

            } else {
                ActivityCompat.requestPermissions(IntroMaster.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.INTERNET, Manifest.permission.ACCESS_FINE_LOCATION
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.READ_PHONE_STATE},
                        PERMISSION_READ_EXTERNAL );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case PERMISSION_READ_EXTERNAL:
                {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                } else {

                }
                return;
            }


        }
    }

    @OnClick({R.id.skip_button, R.id.previousIntro, R.id.nextIntro})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.skip_button:
                startActivity(new Intent(IntroMaster.this, Login.class));
                sessionManager.saveSessionBoolean(sessionManager.SESSION_INTRO, true);
                finish();
                break;
            case R.id.previousIntro:
                pagerViewIntro.setCurrentItem(pagerViewIntro.getCurrentItem() - 1, true);
                break;
            case R.id.nextIntro:
                if (pagerViewIntro.getCurrentItem() < 3){
                    pagerViewIntro.setCurrentItem(pagerViewIntro.getCurrentItem() + 1, true);
                } else {
                    startActivity(new Intent(IntroMaster.this, Login.class));
                    sessionManager.saveSessionBoolean(sessionManager.SESSION_INTRO, true);
                }

                break;
        }
    }

    class SimpleFragmentAdapter extends FragmentPagerAdapter {
        private Context mContext;

        public SimpleFragmentAdapter(Context context, FragmentManager fm){
            super(fm);
            mContext = context;
        }


        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                Fragment fragment = new FragIntro1();
                previousIntro.setVisibility(View.INVISIBLE);
                return fragment;
            } else if(position == 1) {
                Fragment fragment = new FragIntro3();
                previousIntro.setVisibility(View.VISIBLE);
                return  fragment;
            } else if (position == 2){
                Fragment fragment = new FragIntro4();

                return fragment;
            } else{
                Fragment fragment = new FragIntro5();
                return fragment;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}
