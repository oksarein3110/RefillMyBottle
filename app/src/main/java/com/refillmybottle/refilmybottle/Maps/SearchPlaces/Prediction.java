package com.refillmybottle.refilmybottle.Maps.SearchPlaces;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Prediction implements Serializable {
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private String id;
    @SerializedName("matched_substrings")
    private List matchedSubstrings;
    @SerializedName("place_id")
    private String placeId;
    @SerializedName("reference")
    private String reference;
    @SerializedName("terms")
    private List<Terms> terms;
    @SerializedName("types")
    private List<String> types;

    public List<MatchedSubstring> getMatchedSubstrings() {
        return matchedSubstrings;
    }

    public List<String> getTypes() {
        return types;
    }

    public List<Terms> getTerms() {
        return terms;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getReference() {
        return reference;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMatchedSubstrings(List<MatchedSubstring> matchedSubstrings) {
        this.matchedSubstrings = matchedSubstrings;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setTerms(List<Terms> terms) {
        this.terms = terms;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
}
