package com.refillmybottle.refilmybottle.Maps.POJONearby;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class OpeningHoursModels {
    @SerializedName("open_now")
    private boolean openNow;

    public static OpeningHoursModels objectFromData(String str) {

        return new Gson().fromJson(str, OpeningHoursModels.class);
    }

    public boolean isOpenNow() {
        return openNow;
    }

    public void setOpenNow(boolean openNow) {
        this.openNow = openNow;
    }
}
