package com.refillmybottle.refilmybottle.Maps;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.refillmybottle.refilmybottle.R;

public class NotFoundReview extends Fragment {

    public NotFoundReview newInstance(){
        NotFoundReview notFoundReview = new NotFoundReview();
        return notFoundReview;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_not_found_review, container, false);
    }
}
