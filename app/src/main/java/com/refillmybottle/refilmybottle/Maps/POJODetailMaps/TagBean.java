package com.refillmybottle.refilmybottle.Maps.POJODetailMaps;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class TagBean {
    @SerializedName("tags")
    private String tags;


    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
