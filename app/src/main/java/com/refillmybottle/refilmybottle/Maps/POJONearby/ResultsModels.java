package com.refillmybottle.refilmybottle.Maps.POJONearby;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultsModels {
    @SerializedName("geometry")
    private GeometryModels geometry;
    @SerializedName("icon")
    private String icon;
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("opening_hours")
    private OpeningHoursModels openingHours;
    @SerializedName("place_id")
    private String placeId;
    @SerializedName("scope")
    private String scope;
    @SerializedName("reference")
    private String reference;
    @SerializedName("vicinity")
    private String vicinity;
    @SerializedName("photos")
    private List<PhotosModels> photos;
    @SerializedName("alt_ids")
    private List<AltIdsModels> altIds;
    @SerializedName("types")
    private List<String> types;

    public static ResultsModels objectFromData(String str) {

        return new Gson().fromJson(str, ResultsModels.class);
    }

    public GeometryModels getGeometry() {
        return geometry;
    }

    public void setGeometry(GeometryModels geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OpeningHoursModels getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(OpeningHoursModels openingHours) {
        this.openingHours = openingHours;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public List<PhotosModels> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PhotosModels> photos) {
        this.photos = photos;
    }

    public List<AltIdsModels> getAltIds() {
        return altIds;
    }

    public void setAltIds(List<AltIdsModels> altIds) {
        this.altIds = altIds;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
}
