package com.refillmybottle.refilmybottle.Maps.MapsImagesPojo;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class LonlatModelsGrid {
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;

    public static LonlatModelsGrid objectFromData(String str) {

        return new Gson().fromJson(str, LonlatModelsGrid.class);
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
