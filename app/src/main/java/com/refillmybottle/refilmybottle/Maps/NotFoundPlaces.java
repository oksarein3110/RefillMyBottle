package com.refillmybottle.refilmybottle.Maps;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.refillmybottle.refilmybottle.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class NotFoundPlaces extends Fragment {

    @BindView(R.id.suggest_one)
    Button suggestOne;
    Unbinder unbinder;
    double longitude;
    double latitude;

    public static NotFoundPlaces newInstace() {
        NotFoundPlaces notFoundPlaces = new NotFoundPlaces();
        return notFoundPlaces;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_maps_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        Bundle bundle = this.getArguments();
        latitude =  bundle.getDouble("lat");
        longitude = bundle.getDouble("lon");
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.suggest_one)
    public void onViewClicked() {
        Intent intent = new Intent(getContext(), NearbyPlaceSuggest.class);
        Bundle bundle1 = this.getArguments();
        bundle1.putDouble("latt", latitude);
        bundle1.putDouble("lonn", longitude);
        intent.putExtras(bundle1);
        startActivity(intent);

    }
}
