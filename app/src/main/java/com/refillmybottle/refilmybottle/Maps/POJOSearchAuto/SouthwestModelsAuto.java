package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class SouthwestModelsAuto {
    @SerializedName("lat")
    public double lat;
    @SerializedName("lng")
    public double lng;

    public static SouthwestModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, SouthwestModelsAuto.class);
    }
}
