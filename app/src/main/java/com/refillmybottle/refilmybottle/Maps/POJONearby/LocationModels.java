package com.refillmybottle.refilmybottle.Maps.POJONearby;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class LocationModels {
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;

    public static LocationModels objectFromData(String str) {

        return new Gson().fromJson(str, LocationModels.class);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
