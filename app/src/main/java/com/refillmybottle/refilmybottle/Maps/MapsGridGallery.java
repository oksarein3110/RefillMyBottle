package com.refillmybottle.refilmybottle.Maps;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.refillmybottle.refilmybottle.Event.DataModel;
import com.refillmybottle.refilmybottle.Maps.MapsImagesPojo.DataModelsGrid;
import com.refillmybottle.refilmybottle.Maps.MapsImagesPojo.ResponseMapsImg;
import com.refillmybottle.refilmybottle.Maps.POJODetailMaps.ResponseDetailsMaps;
import com.refillmybottle.refilmybottle.Maps.POJOSearchAuto.POJOSearchAutoComplete;
import com.refillmybottle.refilmybottle.Maps.SearchPlaces.GoogleMapAPI;
import com.refillmybottle.refilmybottle.Maps.SearchPlaces.PlacesAutoCompleteAdapter;
import com.refillmybottle.refilmybottle.Maps.SearchPlaces.Prediction;
import com.refillmybottle.refilmybottle.PermissionLocation;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.DataModelsShop;
import com.refillmybottle.refilmybottle.response.DataModels;
import com.refillmybottle.refilmybottle.response.ResponseImagesMaps;
import com.refillmybottle.refilmybottle.response.ResponseMapList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.refillmybottle.refilmybottle.Maps.MapsShowing.MY_PERMISSIONS_REQUEST_LOCATION;
import static java.security.AccessController.getContext;

public class MapsGridGallery extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerClickListener,
        LocationListener  {

    @BindView(R.id.map)
    FrameLayout map;
    @BindView(R.id.search_locations)
    AutoCompleteTextView searchLocations;
    @BindView(R.id.settings_location)
    ImageView settingsLocation;
    @BindView(R.id.mock_location)
    ImageView mockLocation;
    @BindView(R.id.menu_mapss)
    RelativeLayout menuMapss;
    @BindView(R.id.gridDetailMaps)
    RecyclerView gridDetailMaps;
    RequestInterfaces mRequest;
    GoogleMapAPI mGoogleMapAPI;
    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Marker mCurrentLocationMarker;
    private Location mLastLocation;
    double longitude1;
    double latitude1;
    SupportMapFragment mapFragment;
    MarkerOptions markerOptions;
    SessionManager sessionManager;
    private String ID;
    private String marks;
    Context context;

    private ArrayList<DataModelsGrid> dataGrids;
    private AdapterMapsImages adapterGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_grid_gallery);
        ButterKnife.bind(this);
        mRequest = Utils.getApiServices();
        sessionManager = new SessionManager(this);
        mGoogleMapAPI = Utils.getAPINearby();
        context = this;
        gridDetailMaps.bringToFront();

        getImage();
        checkLocationPermission();
        LoadDataSearch();
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if(mapFragment == null){
            FragmentManager fm = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }


        gridDetailMaps.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        gridDetailMaps.setLayoutManager(linearLayoutManager);
        mapFragment.getMapAsync(this);
        mRequest = Utils.getApiServices();
    }

    private void LoadDataSearch() {
        List<Prediction> predictions = new ArrayList<>();
        PlacesAutoCompleteAdapter placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this, predictions);
        searchLocations.setThreshold(1);
        searchLocations.setAdapter(placesAutoCompleteAdapter);
        searchLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                if (item instanceof Prediction){
                    Prediction predictions = (Prediction) item;
                    String idPlace = predictions.getPlaceId();
                    String keyMap = context.getString(R.string.google_maps_key);
                    mGoogleMapAPI.getDetailPlaces(idPlace, keyMap).enqueue(new Callback<POJOSearchAutoComplete>() {
                        @Override
                        public void onResponse(Call<POJOSearchAutoComplete> call, Response<POJOSearchAutoComplete> response) {
                            if(response.isSuccessful()){
                                Double lat = response.body().getResult().getGeometry().getLocation().getLat();
                                Double lon = response.body().getResult().getGeometry().getLocation().getLng();
                                LatLng latLOng = new LatLng(lat,lon);
                                final MarkerOptions markerDef = new MarkerOptions().position(latLOng).title(response.body().getResult().getName().toString());
                                mMap.addMarker(markerDef);
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLOng).zoom(15).build();
                                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            } else {
                                Toast.makeText(MapsGridGallery.this, response.body().getStatus(), Toast.LENGTH_SHORT).show();
                            }


                        }

                        @Override
                        public void onFailure(Call<POJOSearchAutoComplete> call, Throwable t) {

                        }
                    });
                }

            }
        });
    }



    private void getImage(){
    Bundle bundle = getIntent().getExtras();
    ID = bundle.getString("ID");
    mRequest.getImages(ID).enqueue(new Callback<ResponseMapsImg>() {
        @Override
        public void onResponse(Call<ResponseMapsImg> call, Response<ResponseMapsImg> response) {
            ResponseMapsImg responseMapsImg = response.body();
            if (responseMapsImg.getStatus().equals("200")){
                dataGrids = new ArrayList<>();
                for(int i =0;i<responseMapsImg.getDataModelsGrids().length;i++){
                    dataGrids.add(responseMapsImg.getDataModelsGrids()[i]);
                }
                adapterGrid = new AdapterMapsImages(dataGrids);
                gridDetailMaps.setAdapter(adapterGrid);
            } else {

                gridDetailMaps.setVisibility(View.GONE);
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) menuMapss.getLayoutParams();
                params.bottomMargin = 20;
                Toast.makeText(MapsGridGallery.this, responseMapsImg.getMsg().toString(), Toast.LENGTH_SHORT).show();

            }

        }

        @Override
        public void onFailure(Call<ResponseMapsImg> call, Throwable t) {

        }
    });


    }


    @OnClick({R.id.settings_location, R.id.mock_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.settings_location:
                break;
            case R.id.mock_location:
                getMyLocation();
                break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10);
        mLocationRequest.setFastestInterval(20);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        longitude1 = location.getLongitude();
        latitude1 = location.getLatitude();
        mLastLocation = location;
        if (mCurrentLocationMarker != null) {
            mCurrentLocationMarker.remove();
        }




        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        final MarkerOptions markerDef = new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)).title("My Location");
        mMap.addMarker(markerDef).setIcon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("avatar", 150,150)));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latLng.latitude, latLng.longitude)).zoom(17).build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(true);

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
        final List<LatLng> markers = new ArrayList<>();

        String token = sessionManager.getSessionToken().toString();
        mRequest.getMapNear(latLng.longitude, latLng.latitude, token).enqueue(new Callback<ResponseMapList>() {
            @Override
            public void onResponse(Call<ResponseMapList> call, final Response<ResponseMapList> response) {
                if (response.isSuccessful()){
                        try{
                            /*mMap.clear();*/

                            for(int i=0;i<response.body().getData().size();i++){
                                final String id;
                                final float distance;
                                String lattitude = response.body().getData().get(i).getLatitude().toString();
                                String longitude = response.body().getData().get(i).getLongitude().toString();
                                final String name_place = response.body().getData().get(i).getNama().toString();
                                final String marks = response.body().getData().get(i).getMark().toString();
                                final String star = response.body().getData().get(i).getStar().toString() + " Star";
                                id = response.body().getData().get(i).getId().toString();

                                LatLng lokasi = new LatLng(Double.parseDouble(lattitude),Double.parseDouble(longitude));
                                markers.add(lokasi);

                                Location loc1 = new Location("");
                                loc1.setLatitude(latLng.latitude);
                                loc1.setLongitude(latLng.longitude);

                                Location loc2 = new Location("");
                                loc2.setLatitude(Double.parseDouble(response.body().getData().get(i).getLatitude()));
                                loc2.setLongitude(Double.parseDouble(response.body().getData().get(i).getLongitude()));

                                distance = loc1.distanceTo(loc2) / 1609;

                                markerOptions = new MarkerOptions().position(markers.get(markers.size()-1)).snippet(id);




                                if(id.equals(ID)) {
                                    if (marks.equals("water")){
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.waterms));
                                    } else if (marks.equals("premium")){
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.premiumms));
                                    } else {
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.dollarsm));
                                    }

                                } else {
                                    BitmapDrawable bitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.pin);
                                    Bitmap b = bitmapDrawable.getBitmap();
                                    Bitmap small = b.createScaledBitmap(b, 150, 150, false);
                                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(small));
                                }
                                mMap.addMarker(markerOptions);

                                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                    @Override
                                    public boolean onMarkerClick(Marker marker) {

                                        Intent intent = new Intent(MapsGridGallery.this, DetailMaps.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("ID", marker.getSnippet().toString());
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                        return false;
                                    }
                                });
                            }


                        } catch (Exception e){
                            e.printStackTrace();
                        }


                }

            }

            @Override
            public void onFailure(Call<ResponseMapList> call, Throwable t) {

            }
        });


    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;



        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.styles_maps));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(false);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(false);
        }

    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public void checkLocationPermission() {
        LocationManager locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){

        } else {
            Toast.makeText(MapsGridGallery.this, "Enable Location!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Izin diberikan.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Izin ditolak.
                    Toast.makeText(MapsGridGallery.this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void getMyLocation() {
        LatLng latLng = new LatLng(latitude1,longitude1);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
        mMap.moveCamera(cameraUpdate);
    }

    public Bitmap resizeMapIcons(String iconName,int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

}
