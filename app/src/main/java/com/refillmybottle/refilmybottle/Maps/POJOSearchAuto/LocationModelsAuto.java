package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class LocationModelsAuto {
    @SerializedName("lat")
    public double lat;
    @SerializedName("lng")
    public double lng;

    public static LocationModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, LocationModelsAuto.class);
    }

    public double getLng() {
        return lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
