package com.refillmybottle.refilmybottle.Maps;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.refillmybottle.refilmybottle.Maps.POJOREVIEW.DataModelsAuto;
import com.refillmybottle.refilmybottle.Maps.POJOREVIEW.ModelReview;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragReview extends Fragment {

    @BindView(R.id.recyclerReviews)
    RecyclerView recyclerReviews;
    Unbinder unbinder;
    private ArrayList<DataModelsAuto> dataReview;
    private AdapterReviewMaps adapterReviewMaps;
    RequestInterfaces mReq;
    SessionManager sessionManager;

    public static FragReview newInstance() {
        FragReview fragReview = new FragReview();
        return fragReview;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_frag_review, container, false);
        unbinder = ButterKnife.bind(this, view);
        mReq = Utils.getApiServices();
        sessionManager = new SessionManager(getContext());
        initViews();
        return view;
    }

    private void initViews() {
        recyclerReviews.setHasFixedSize(true);
        recyclerReviews.setLayoutManager(new LinearLayoutManager(getContext()));
        loadJson();
    }

    private void loadJson() {
        mReq.getReview(sessionManager.getSessionMapSelectDetail().toString()).enqueue(new Callback<ModelReview>() {
            @Override
            public void onResponse(Call<ModelReview> call, Response<ModelReview> response) {
                if(response.isSuccessful()){
                    if (response.body().getData() == null){
                        ((DetailMaps)getActivity()).NotFound();
                    } else {
                        ModelReview modelReview = response.body();
                        dataReview = new ArrayList<>(Arrays.asList(modelReview.getData()));
                        adapterReviewMaps = new AdapterReviewMaps(dataReview);
                        recyclerReviews.setAdapter(adapterReviewMaps);
                    }
                } else {
                    ((DetailMaps)getActivity()).NotFound();
                }
            }

            @Override
            public void onFailure(Call<ModelReview> call, Throwable t) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
