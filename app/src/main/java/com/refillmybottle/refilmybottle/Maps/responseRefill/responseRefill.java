package com.refillmybottle.refilmybottle.Maps.responseRefill;

import com.google.gson.annotations.SerializedName;

public class responseRefill {


    /**
     * status : 200
     * msg : Refill on Alfamart Beruang Success!
     * pts : 10
     * review : FALSE
     */

    @SerializedName("status")
    private String status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("pts")
    private String pts;
    @SerializedName("review")
    private String review;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getPts() {
        return pts;
    }

    public void setPts(String pts) {
        this.pts = pts;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
