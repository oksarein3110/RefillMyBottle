package com.refillmybottle.refilmybottle.Maps;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.Maps.POJOREVIEW.DataModelsAuto;
import com.refillmybottle.refilmybottle.R;

import java.util.ArrayList;

public class AdapterReviewMaps extends RecyclerView.Adapter<AdapterReviewMaps.ViewHolder> {

    private Context context;
    private ArrayList<DataModelsAuto> data;

    public AdapterReviewMaps(ArrayList<DataModelsAuto> data){
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterReviewMaps.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reviews_places, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterReviewMaps.ViewHolder holder, int position) {
    holder.comment.setText(data.get(position).getKomentar().toString());
    holder.username.setText(data.get(position).getNama().toString());
    holder.stars.setRating(Float.parseFloat(data.get(position).getStar().toString()));
    holder.date.setText(data.get(position).getDatetime().toString());
    holder.stars.setIsIndicator(false);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView date, username, comment;
        private RatingBar stars;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.dateTimeReview);
            username = itemView.findViewById(R.id.UserNameName);
            comment = itemView.findViewById(R.id.commentReviewMaps);
            stars = itemView.findViewById(R.id.ratingPlaceReview);
        }
    }
}
