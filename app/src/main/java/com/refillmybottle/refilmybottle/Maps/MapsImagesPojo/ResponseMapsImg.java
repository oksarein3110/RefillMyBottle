package com.refillmybottle.refilmybottle.Maps.MapsImagesPojo;


import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseMapsImg {
    private DataModelsGrid[] data;
    @SerializedName("msg")
    private String msg;
    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }
    public String getMsg() {
        return msg;
    }
    public DataModelsGrid[] getDataModelsGrids() {
        return data;
    }


}
