package com.refillmybottle.refilmybottle.Maps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.refillmybottle.refilmybottle.Maps.MapsImagesPojo.DataModelsGrid;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.DataModelsShop;
import com.refillmybottle.refilmybottle.response.DataModels;
import com.refillmybottle.refilmybottle.response.ResponseImagesMaps;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by abah on 27/08/18.
 */

public class AdapterMapsImages extends RecyclerView.Adapter<AdapterMapsImages.ViewHolder> {
    private Context context;
    private ArrayList<DataModelsGrid> data;
    private SessionManager sessionManager;
    Double longitude, lattitude;

    public AdapterMapsImages(ArrayList<DataModelsGrid> dataGrid) {
        this.data = dataGrid;

    }

    @NonNull
    @Override
    public AdapterMapsImages.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowgridmaps, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMapsImages.ViewHolder holder, int position) {

        final String images_uri = data.get(position).getImg().toString();
        final Location location = getAddress();
        final Location location1 = new Location("");
        location1.setLongitude(Double.parseDouble(data.get(position).getLongitude()));
        location1.setLatitude(Double.parseDouble(data.get(position).getLatitude()));
        final String distances = distance(location,location1);
        Picasso.with(context).load(images_uri).into(holder.images);
        holder.title.setText(data.get(position).getName().toString());
        holder.distance.setText(distances);
        holder.cost.setText(data.get(position).getStar());


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title, distance, cost;
        private ImageView images;
        public ViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            images = itemView.findViewById(R.id.imageGrid);
            title = itemView.findViewById(R.id.titlePlacetxt);
            distance = itemView.findViewById(R.id.farstar);
            cost = itemView.findViewById(R.id.costPlace);

        }
    }

    public Location getAddress() {



        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        LocationManager locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            longitude = location.getLongitude();
            lattitude = location.getLatitude();

        } else {
            Toast.makeText(context, "Null", Toast.LENGTH_SHORT).show();
        }

        Location locations = new Location("");
        locations.setLongitude(longitude);
        locations.setLatitude(lattitude);

        return locations;

    }

    public String distance(Location from, Location dest){

        Float calcDista = (from.distanceTo(dest))/1000;
        DecimalFormat format = new DecimalFormat("#.##");
        String Distance = format.format(calcDista) + "Km";

        return Distance;
    }



}
