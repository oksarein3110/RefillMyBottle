package com.refillmybottle.refilmybottle.Maps.SearchPlaces;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Predictions {
    @SerializedName("status")
    private String status;
    @SerializedName("predictions")
    private List<Prediction> predictions;

    public List<Prediction> getPredictions() {
        return predictions;
    }

    public String getStatus() {
        return status;
    }

    public void setPredictions(List<Prediction> predictions) {
        this.predictions = predictions;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
