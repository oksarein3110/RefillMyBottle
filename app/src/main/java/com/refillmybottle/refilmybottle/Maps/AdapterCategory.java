package com.refillmybottle.refilmybottle.Maps;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.Maps.POJODetailMaps.TagBean;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.ViewHolder>{
    private Context context;
    private ArrayList<TagBean> tag;
    private SessionManager sessionManager;

    public AdapterCategory(ArrayList<TagBean> tag){

        this.tag = tag;
    }



    @NonNull
    @Override
    public AdapterCategory.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category_maps, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCategory.ViewHolder holder, int position) {
        holder.textCat.setText(tag.get(position).getTags().toString());

    }

    @Override
    public int getItemCount() {
        return tag.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textCat;
        public ViewHolder(View view){
            super(view);
            context = view.getContext();
            textCat = view.findViewById(R.id.text_category_float);
        }
    }
}
