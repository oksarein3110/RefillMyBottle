package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ReviewsModelsAuto {
    @SerializedName("author_name")
    public String authorName;
    @SerializedName("author_url")
    public String authorUrl;
    @SerializedName("language")
    public String language;
    @SerializedName("profile_photo_url")
    public String profilePhotoUrl;
    @SerializedName("rating")
    public int rating;
    @SerializedName("relative_time_description")
    public String relativeTimeDescription;
    @SerializedName("text")
    public String text;
    @SerializedName("time")
    public int time;

    public static ReviewsModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, ReviewsModelsAuto.class);
    }
}
