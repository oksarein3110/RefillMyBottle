package com.refillmybottle.refilmybottle.Maps;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.refillmybottle.refilmybottle.Maps.MapsImagesPojo.DataModelsGrid;
import com.refillmybottle.refilmybottle.Maps.MapsImagesPojo.ResponseMapsImg;
import com.refillmybottle.refilmybottle.Maps.POJOSearchAuto.POJOSearchAutoComplete;
import com.refillmybottle.refilmybottle.Maps.SearchPlaces.GoogleMapAPI;
import com.refillmybottle.refilmybottle.Maps.SearchPlaces.PlacesAutoCompleteAdapter;
import com.refillmybottle.refilmybottle.Maps.SearchPlaces.Prediction;
import com.refillmybottle.refilmybottle.PermissionLocation;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.response.ResponseMapList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MapsShowing extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerClickListener,
        LocationListener {

    @BindView(R.id.settings_location)
    ImageView settingsLocation;
    @BindView(R.id.mock_location)
    ImageView mockLocation;
    @BindView(R.id.recyclerBranch)
    RecyclerView recyclerBranch;
    @BindView(R.id.menu_mapss)
    RelativeLayout menuMapss;
    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    GoogleMapAPI mGoogleMapAPI;
    private LocationRequest mLocationRequest;
    private Marker mCurrentLocationMarker;
    private Location mLastLocation;
    RequestInterfaces mRequest;
    double latitude;
    double longitude;
    SupportMapFragment mapFragment;
    AutoCompleteTextView search_places;
    MarkerOptions markerOptions;
    SessionManager sessionManager;

    int click = 0;

    private ArrayList<DataModelsGrid> dataGrids;
    private AdapterMapsImages adapterGrid;


    private OnFragmentInteractionListener mListener;


    public static MapsShowing newInstance() {
        MapsShowing fragment = new MapsShowing();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_maps_showing, container, false);
        search_places = view.findViewById(R.id.search_locations);


        checkLocationPermission();
        mGoogleMapAPI = Utils.getAPINearby();
        sessionManager = new SessionManager(getContext());
        LoadDataSearch();
        ButterKnife.bind(this, view);

        recyclerBranch.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerBranch.setLayoutManager(linearLayoutManager);
        recyclerBranch.setVisibility(View.GONE);

        menuMapss.setBottom(50);


        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fm = getChildFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }

        mapFragment.getMapAsync(this);
        mRequest = Utils.getApiServices();

        return view;
    }

    private void LoadDataSearch() {
        final List<Prediction> predictions = new ArrayList<>();
        PlacesAutoCompleteAdapter placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getContext(), predictions);
        search_places.setThreshold(1);
        search_places.setAdapter(placesAutoCompleteAdapter);
        search_places.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                if (item instanceof Prediction) {
                    Prediction predictions = (Prediction) item;
                    String idPlace = predictions.getPlaceId();
                    String keyMap = getContext().getString(R.string.google_maps_key);
                    mGoogleMapAPI.getDetailPlaces(idPlace, keyMap).enqueue(new Callback<POJOSearchAutoComplete>() {
                        @Override
                        public void onResponse(Call<POJOSearchAutoComplete> call, Response<POJOSearchAutoComplete> response) {
                            if (response.isSuccessful()) {
                                Double lat = response.body().getResult().getGeometry().getLocation().getLat();
                                Double lon = response.body().getResult().getGeometry().getLocation().getLng();
                                LatLng latLOng = new LatLng(lat, lon);
                                final MarkerOptions markerDef = new MarkerOptions().position(latLOng).title(response.body().getResult().getName().toString());
                                mMap.addMarker(markerDef);
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLOng).zoom(15).build();
                                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            } else {
                                Toast.makeText(getContext(), response.body().getStatus().toString(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<POJOSearchAutoComplete> call, Throwable t) {

                        }
                    });
                }

            }
        });
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10);
        mLocationRequest.setFastestInterval(20);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(final Location location) {
        mLastLocation = location;
        if (mCurrentLocationMarker != null) {
            mCurrentLocationMarker.remove();
        }

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
//                handler.postDelayed(this, 1000);
//            }
//        }, 999);


        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        final MarkerOptions markerDef = new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)).snippet("my");
        mMap.addMarker(markerDef).setIcon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("avatar", 150, 150)));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latLng.latitude, latLng.longitude)).zoom(17).build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(true);



        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
        final List<LatLng> markers = new ArrayList<>();

        String token = sessionManager.getSessionToken().toString();
        mRequest.getMapNear(longitude, latitude, token).enqueue(new Callback<ResponseMapList>() {
            @Override
            public void onResponse(Call<ResponseMapList> call, final Response<ResponseMapList> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() == null) {
                        NotFoundPlaces notFoundPlaces = NotFoundPlaces.newInstace();
                        Bundle bundle = new Bundle();
                        bundle.putDouble("lat", latitude);
                        bundle.putDouble("lon", longitude);
                        notFoundPlaces.setArguments(bundle);
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout, notFoundPlaces);

                        fragmentTransaction.commit();

                    } else {

                        try {
                            /*mMap.clear();*/

                            for (int i = 0; i < response.body().getData().size(); i++) {
                                final String id;
                                final float distance;
                                String lattitude = response.body().getData().get(i).getLatitude().toString();
                                String longitude = response.body().getData().get(i).getLongitude().toString();
                                final String name_place = response.body().getData().get(i).getNama().toString();
                                final String marks = response.body().getData().get(i).getMark().toString();
                                final String star = response.body().getData().get(i).getStar().toString() + " Star";
                                id = response.body().getData().get(i).getId().toString();

                                LatLng lokasi = new LatLng(Double.parseDouble(lattitude), Double.parseDouble(longitude));
                                markers.add(lokasi);

                                Location loc1 = new Location("");
                                loc1.setLatitude(location.getLatitude());
                                loc1.setLongitude(location.getLongitude());

                                Location loc2 = new Location("");
                                loc2.setLatitude(Double.parseDouble(response.body().getData().get(i).getLatitude()));
                                loc2.setLongitude(Double.parseDouble(response.body().getData().get(i).getLongitude()));

                                distance = loc1.distanceTo(loc2) / 1609;

                                markerOptions = new MarkerOptions().position(markers.get(markers.size() - 1)).snippet(id);



                                    if (marks.equals("water")) {
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pinwater));

                                    } else if (marks.equals("premium")) {
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpremium));

                                    } else {
                                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pindollar));

                                    }

                                    mMap.addMarker(markerOptions);

//                                            BitmapDrawable bitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.pin);
//                                            Bitmap b = bitmapDrawable.getBitmap();
//                                            Bitmap small = b.createScaledBitmap(b, 150, 150, false);
//                                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(small));
//


                                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                    @Override
                                    public boolean onMarkerClick(Marker marker) {
                                        marker.hideInfoWindow();
                                        String ID = marker.getSnippet().toString();

                                            click=click+1;

                                            if(click==1){
                                                sessionManager.saveSessionStr(sessionManager.SESSION_TEMP_SELECTPLACE, ID);
                                                menuMapss.setBottom(125);
                                                recyclerBranch.setVisibility(View.VISIBLE);
                                                getImage(ID);
                                            } else if (click>=2){
                                                sessionManager.saveSessionStr(sessionManager.SESSION_MAP_SELECT_DETAIL, marker.getSnippet());
                                                Intent intent = new Intent(getContext(), DetailMaps.class);

                                                click=0;
                                                startActivity(intent);


                                            }

                                            if(ID.equals(sessionManager.getSessionTempSelectplace())){
                                                if (marks.equals("water")) {
                                                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.waterms));

                                                } else if (marks.equals("premium")) {
                                                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.premiumms));

                                                } else if(marks.equals("dollar")) {
                                                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.dollarsm));
                                                }

                                            } else if(ID.equals("my")) {
                                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.avatar));
                                            }

                                            mMap.addMarker(markerOptions);

                                        return true;
                                    }
                                });
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

            }

            @Override
            public void onFailure(Call<ResponseMapList> call, Throwable t) {

            }
        });



    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);

        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.styles_maps));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(false);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(false);
        }


    }


    private void getImage(String IDs){
        mRequest.getImages(IDs).enqueue(new Callback<ResponseMapsImg>() {
            @Override
            public void onResponse(Call<ResponseMapsImg> call, Response<ResponseMapsImg> response) {
                ResponseMapsImg responseMapsImg = response.body();
                if (responseMapsImg.getStatus().equals("200")){
                    dataGrids = new ArrayList<>();
                    for(int i =0;i<responseMapsImg.getDataModelsGrids().length;i++){
                        dataGrids.add(responseMapsImg.getDataModelsGrids()[i]);
                    }
                    adapterGrid = new AdapterMapsImages(dataGrids);
                    recyclerBranch.setAdapter(adapterGrid);
                } else {

                    recyclerBranch.setVisibility(View.GONE);
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) menuMapss.getLayoutParams();
                    params.bottomMargin = 20;
                    Toast.makeText(getContext(), responseMapsImg.getMsg().toString(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<ResponseMapsImg> call, Throwable t) {

            }
        });


    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public void checkLocationPermission() {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(getContext().LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

        } else {
            PermissionLocation permissionLocation = new PermissionLocation();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, permissionLocation);
            fragmentTransaction.commit();
        }
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }



    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Izin diberikan.
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Izin ditolak.
                    Toast.makeText(getContext(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @OnClick({R.id.settings_location, R.id.mock_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.settings_location:
                break;
            case R.id.mock_location:
                getMyLocation();
                break;
        }
    }

    public void getMyLocation() {
        LatLng latLng = new LatLng(latitude, longitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
        mMap.moveCamera(cameraUpdate);
    }

    public void RecentLoc(LatLng location) {
        location = new LatLng(latitude, longitude);
    }

    public Bitmap resizeMapIcons(String iconName, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(iconName, "drawable", getContext().getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

    @Override
    public void onStart() {
        super.onStart();
        sessionManager.saveSessionStr(sessionManager.SESSION_TEMP_SELECTPLACE, "");
    }
}
