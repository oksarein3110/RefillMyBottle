package com.refillmybottle.refilmybottle.Maps.POJODetailMaps;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ReviewModels {
    @SerializedName("id_review")
    private String idReview;
    @SerializedName("nama")
    private String nama;
    @SerializedName("star")
    private String star;
    @SerializedName("komentar")
    private String komentar;
    @SerializedName("datetime")
    private String datetime;



    public String getIdReview() {
        return idReview;
    }

    public void setIdReview(String idReview) {
        this.idReview = idReview;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
