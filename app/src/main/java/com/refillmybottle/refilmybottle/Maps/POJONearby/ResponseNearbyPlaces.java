package com.refillmybottle.refilmybottle.Maps.POJONearby;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseNearbyPlaces {

    @SerializedName("status")
    private String status;
    @SerializedName("html_attributions")
    private List<?> htmlAttributions;
    @SerializedName("results")
    private List<ResultsModels> results;

    public static ResponseNearbyPlaces objectFromData(String str) {

        return new Gson().fromJson(str, ResponseNearbyPlaces.class);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<?> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<?> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public void setResults(List<ResultsModels> results) {
        this.results = results;
    }

    public List<ResultsModels> getResults() {
        return results;
    }
}
