package com.refillmybottle.refilmybottle.Maps.SearchPlaces;

import com.google.android.gms.maps.model.LatLng;
import com.refillmybottle.refilmybottle.Maps.POJONearby.ResponseNearbyPlaces;
import com.refillmybottle.refilmybottle.Maps.POJOSearchAuto.POJOSearchAutoComplete;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleMapAPI {
    @GET("place/autocomplete/json")
    public Call<Predictions> getPlacesAutoComplete(
            @Query("input") String input,
            @Query("types") String types,
            @Query("language") String language,
            @Query("key") String key
    );

    @GET("place/nearbysearch/json")
    public Call<ResponseNearbyPlaces> getNearbyCoffe(
            @Query("location") String location,
            @Query("radius") int radius,
            @Query("type") String type,
            @Query("key") String key
    );

    @GET("place/details/json")
    public Call<POJOSearchAutoComplete> getDetailPlaces(
            @Query("placeid") String placeid,
            @Query("key") String key
    );
}
