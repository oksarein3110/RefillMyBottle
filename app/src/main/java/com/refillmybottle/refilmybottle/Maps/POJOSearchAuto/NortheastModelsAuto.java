package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class NortheastModelsAuto {
    @SerializedName("lat")
    public double lat;
    @SerializedName("lng")
    public double lng;

    public static NortheastModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, NortheastModelsAuto.class);
    }
}
