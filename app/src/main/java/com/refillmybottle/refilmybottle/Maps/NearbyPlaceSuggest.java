package com.refillmybottle.refilmybottle.Maps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Adapter;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.refillmybottle.refilmybottle.Maps.POJONearby.ResponseNearbyPlaces;
import com.refillmybottle.refilmybottle.Maps.POJONearby.ResultsModels;
import com.refillmybottle.refilmybottle.Maps.SearchPlaces.GoogleMapAPI;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NearbyPlaceSuggest extends AppCompatActivity  {


    @BindView(R.id.recyclerNearby)
    RecyclerView recyclerNearby;
    GoogleMapAPI mGoogleApiClient;
    Context context;
    private List<ResultsModels> results;
    private AdapterNearbyPlaces adapterNearbyPlaces;
    private SwipeRefreshLayout refreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_place_suggest);
        ButterKnife.bind(this);
        context = this;
        mGoogleApiClient = Utils.getAPINearby();

        LoadJson();

    }

    private void LoadJson() {
        Bundle bundle = getIntent().getExtras();

        Double lonNow = bundle.getDouble("lonn");
        Double latNow = bundle.getDouble("latt");
        String location = latNow + "," + lonNow;
        String keyword = "cafe";
        String keyMap = context.getString(R.string.google_maps_key);
    mGoogleApiClient.getNearbyCoffe(location, 3000, keyword, keyMap).enqueue(new Callback<ResponseNearbyPlaces>() {
        @Override
        public void onResponse(Call<ResponseNearbyPlaces> call, Response<ResponseNearbyPlaces> response) {
            ResponseNearbyPlaces responseNearbyPlaces = response.body();
            results = new ArrayList<>();
            results = responseNearbyPlaces.getResults();
            adapterNearbyPlaces = new AdapterNearbyPlaces(results);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerNearby.setLayoutManager(linearLayoutManager);
            recyclerNearby.setAdapter(adapterNearbyPlaces);

            
        }

        @Override
        public void onFailure(Call<ResponseNearbyPlaces> call, Throwable t) {

        }
    });
    }

}
