package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class POJOSearchAutoComplete {


    @SerializedName("result")
    public ResultModelsAuto result;
    @SerializedName("status")
    public String status;
    @SerializedName("html_attributions")
    public List<?> htmlAttributions;

    public static POJOSearchAutoComplete objectFromData(String str) {

        return new Gson().fromJson(str, POJOSearchAutoComplete.class);
    }

    public ResultModelsAuto getResult() {
        return result;
    }

    public String getStatus() {
        return status;
    }

    public List<?> getHtmlAttributions() {
        return htmlAttributions;
    }
}
