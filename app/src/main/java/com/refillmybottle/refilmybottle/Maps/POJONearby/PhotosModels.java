package com.refillmybottle.refilmybottle.Maps.POJONearby;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhotosModels {
    @SerializedName("height")
    private int height;
    @SerializedName("photo_reference")
    private String photoReference;
    @SerializedName("width")
    private int width;
    @SerializedName("html_attributions")
    private List<?> htmlAttributions;

    public static PhotosModels objectFromData(String str) {

        return new Gson().fromJson(str, PhotosModels.class);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public List<?> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<?> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }
}
