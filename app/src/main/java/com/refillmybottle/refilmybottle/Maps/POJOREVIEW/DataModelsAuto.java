package com.refillmybottle.refilmybottle.Maps.POJOREVIEW;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class DataModelsAuto {
    @SerializedName("id_review")
    private String id_review;
    @SerializedName("nama")
    private String nama;
    @SerializedName("star")
    private String star;
    @SerializedName("komentar")
    private  String komentar;
    @SerializedName("datetime")
    private String datetime;

    public String getStar() {
        return star;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getId_review() {
        return id_review;
    }

    public String getKomentar() {
        return komentar;
    }

    public String getNama() {
        return nama;
    }

}
