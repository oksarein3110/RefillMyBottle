package com.refillmybottle.refilmybottle.Maps;

import android.app.Application;

public class dataOverview {
    public static String details;
    public static String ID;
    public static String jamBuka;
    public static String jamTutup;
    public static String openStatus;
    public static String poinPlaces;
    public static String harga;

    public static void setHarga(String harga) {
        dataOverview.harga = harga;
    }

    public static void setJamBuka(String jamBuka) {
        dataOverview.jamBuka = jamBuka;
    }

    public static void setJamTutup(String jamTutup) {
        dataOverview.jamTutup = jamTutup;
    }

    public static void setOpenStatus(String openStatus) {
        dataOverview.openStatus = openStatus;
    }

    public static void setPoinPlaces(String poinPlaces) {
        dataOverview.poinPlaces = poinPlaces;
    }

    public static void setID(String ID) {
        dataOverview.ID = ID;
    }

    public static void setDetails(String details) {
        dataOverview.details = details;
    }

    public static String getDetails() {
        return details;
    }

    public static String getID() {
        return ID;
    }

    public static String getJamBuka() {
        return jamBuka;
    }

    public static String getJamTutup() {
        return jamTutup;
    }

    public static String getOpenStatus() {
        return openStatus;
    }

    public static String getPoinPlaces() {
        return poinPlaces;
    }

    public static String getHarga() {
        return harga;
    }
}
