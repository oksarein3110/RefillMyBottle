package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class GeometryModelsAuto {
    @SerializedName("location")
    public LocationModelsAuto location;
    @SerializedName("viewport")
    public ViewportModelsAuto viewport;

    public static GeometryModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, GeometryModelsAuto.class);
    }

    public LocationModelsAuto getLocation() {
        return location;
    }

    public ViewportModelsAuto getViewport() {
        return viewport;
    }

    public void setLocation(LocationModelsAuto location) {
        this.location = location;
    }

    public void setViewport(ViewportModelsAuto viewport) {
        this.viewport = viewport;
    }

}
