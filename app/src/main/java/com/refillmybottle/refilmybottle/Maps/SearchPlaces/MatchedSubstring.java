package com.refillmybottle.refilmybottle.Maps.SearchPlaces;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MatchedSubstring implements Serializable {
    @SerializedName("length")
    private int length;
    @SerializedName("offset")
    private int offset;

    public int getOffset() {
        return offset;
    }

    public int getLength() {
        return length;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
