package com.refillmybottle.refilmybottle.Maps;

import android.content.Context;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.common.internal.Objects;
import com.refillmybottle.refilmybottle.Maps.POJODetailMaps.ResponseDetailsMaps;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragOverview extends Fragment {


    @BindView(R.id.content_desc_detail_map)
    TextView contentDescDetailMap;
    @BindView(R.id.refiling_status)
    TextView refilingStatus;
    @BindView(R.id.openning_hours_label)
    TextView openningHoursLabel;
    @BindView(R.id.opening_status_now)
    TextView openingStatusNow;
    @BindView(R.id.work_hour)
    TextView workHour;
    @BindView(R.id.average_cost_value)
    TextView averageCostValue;
    Unbinder unbinder;
    Context context;

    SessionManager sessionManager;
    RequestInterfaces mReq;
    String detailPlace, harga, poin, openStatus, jamBuka, jamTutup;
    public static FragOverview newInstance() {
        FragOverview fragOverview = new FragOverview();
        return fragOverview;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_frag_overview, container, false);
        mReq = Utils.getApiServices();
        context = getContext();
        sessionManager = new SessionManager(getContext());
        unbinder = ButterKnife.bind(this, view);
        loadData();

        return view;
    }



    public void loadData() {
        mReq.getDetailMaps(sessionManager.getSessionMapSelectDetail().toString()).enqueue(new Callback<ResponseDetailsMaps>() {
            @Override
            public void onResponse(Call<ResponseDetailsMaps> call, Response<ResponseDetailsMaps> response) {
                ResponseDetailsMaps responseDetailsMaps = response.body();
                for(int i=0; i < responseDetailsMaps.getData().size(); i++){
                    detailPlace = responseDetailsMaps.getData().get(i).getDetail().toString();
                    harga = responseDetailsMaps.getData().get(i).getHarga().toString();
                    poin = responseDetailsMaps.getData().get(i).getPoin().toString();
                    jamBuka = responseDetailsMaps.getData().get(i).getJamBuka().toString();
                    jamTutup = responseDetailsMaps.getData().get(i).getJamTutup().toString();

                    Date hour1 = convertTime(jamBuka);
                    Date hour2 = convertTime(jamTutup);

                    int buka = hour1.getHours();
                    int tutup = hour2.getHours();

                    Calendar calendar = Calendar.getInstance();
                    int currentH = calendar.get(Calendar.HOUR_OF_DAY);


                    if(currentH > buka && currentH < tutup){
                        openStatus = "Open Now  ";
                        openingStatusNow.setTextColor(Color.GREEN);
                    } else {
                        openStatus = "Close Now  ";
                        openingStatusNow.setTextColor(Color.RED);
                    }


                    contentDescDetailMap.setText(detailPlace);
                    refilingStatus.setText(poin + " Points");
                    openingStatusNow.setText(openStatus);
                    workHour.setText(jamBuka + " - " + jamTutup);

                }
            }

            @Override
            public void onFailure(Call<ResponseDetailsMaps> call, Throwable t) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    public static Date convertTime(String hour){
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");
        Date date = new Date();
        try {
            date = dateFormat.parse(hour);
        } catch (ParseException e){
            e.printStackTrace();
        }
        return date;
    }
}
