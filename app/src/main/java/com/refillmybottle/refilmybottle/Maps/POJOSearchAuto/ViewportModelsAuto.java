package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ViewportModelsAuto {
    @SerializedName("northeast")
    public NortheastModelsAuto northeast;
    @SerializedName("southwest")
    public SouthwestModelsAuto southwest;

    public static ViewportModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, ViewportModelsAuto.class);
    }
}
