package com.refillmybottle.refilmybottle.Maps;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.refillmybottle.refilmybottle.FragmentParent;
import com.refillmybottle.refilmybottle.Maps.POJONearby.ResponseNearbyPlaces;
import com.refillmybottle.refilmybottle.Maps.POJONearby.ResultsModels;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.newrefill.FormRefill;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abah on 31/07/18.
 */

public class AdapterNearbyPlaces extends RecyclerView.Adapter<AdapterNearbyPlaces.ViewHolder> {


    private Context context;
    private List<ResultsModels> results;
    private SessionManager sessionManager;
    private SwipeRefreshLayout refreshLayout;

    public AdapterNearbyPlaces(List<ResultsModels> results) {
        this.results = results;
    }





    @Override
    public AdapterNearbyPlaces.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_nearby_places, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterNearbyPlaces.ViewHolder holder, final int i) {
        Double lat = 0.0;
        Double lon = 0.0;
        final String name_places = results.get(i).getName().toString();
        final String address = results.get(i).getVicinity().toString();
        lat= results.get(i).getGeometry().getLocation().getLat();
        lon = results.get(i).getGeometry().getLocation().getLng();
        Double latt;
        Double lonn;
        Location locNow = new Location("");
//        Boolean open = results.get(i).getOpeningHours().isOpenNow();

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationManager locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            latt = location.getLongitude();
            lonn = location.getLatitude();
            locNow.setLatitude(latt);
            locNow.setLongitude(lonn);
//            Toast.makeText(DetailMaps.this, String.format("%.5f", longitude) + " " + String.format("%.5f", lattitude), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Null", Toast.LENGTH_SHORT).show();
        }







        Location locTarget = new Location("");
        locTarget.setLatitude(lat);
        locTarget.setLongitude(lon);

//        DecimalFormat decimalFormat = new DecimalFormat(".##");
        Float calcDista = (locNow.distanceTo(locTarget))/10000000;
        DecimalFormat format = new DecimalFormat("#.##");
        String Distance = format.format(calcDista) + "Km";

        holder.name_place.setText(name_places);
        holder.address.setText(address);
//        if (open.equals(true)){
//            holder.status_open.setText("Maybe Open");
//        } else {
//            holder.status_open.setText("Maybe Close");
//        }
        holder.distance.setText(Distance);
        holder.nerbyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FragmentParent.class);
                intent.putExtra("code", 1);
                ((Activity)context).startActivity(intent);
                FragmentParent fr = new FragmentParent();

                Toast.makeText(context, name_places + " " + address, Toast.LENGTH_SHORT).show();
                ((Activity) context).finish();
            }
        });


    }



    @Override
    public int getItemCount() {
        return results.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView name_place, status_open, address, distance;
        private RelativeLayout nerbyContent;
        public ViewHolder(View view){
            super (view);
            context = view.getContext();
            name_place = view.findViewById(R.id.name_placesNear);
            status_open = view.findViewById(R.id.status_openNear);
            address = view.findViewById(R.id.address_textNear);
            distance = view.findViewById(R.id.distance_value);
            nerbyContent = view.findViewById(R.id.placesNearbyContent);
        }
    }
}
