package com.refillmybottle.refilmybottle.Maps.POJONearby;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class AltIdsModels {
    @SerializedName("place_id")
    private String placeId;
    @SerializedName("scope")
    private String scope;

    public static AltIdsModels objectFromData(String str) {

        return new Gson().fromJson(str, AltIdsModels.class);
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
