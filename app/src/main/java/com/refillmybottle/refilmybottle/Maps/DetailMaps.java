package com.refillmybottle.refilmybottle.Maps;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.refillmybottle.refilmybottle.Maps.POJODetailMaps.DataBean;
import com.refillmybottle.refilmybottle.Maps.POJODetailMaps.ResponseDetailsMaps;
import com.refillmybottle.refilmybottle.Maps.POJODetailMaps.TagBean;
import com.refillmybottle.refilmybottle.Maps.POJOREVIEW.DataModelsAuto;
import com.refillmybottle.refilmybottle.Maps.POJOREVIEW.ModelReview;
import com.refillmybottle.refilmybottle.Maps.responseRefill.responseRefill;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class DetailMaps extends AppCompatActivity {

    @BindView(R.id.pic_wall_detail)
    ImageView picWallDetail;
    @BindView(R.id.back_detail)
    ImageView backDetail;
    @BindView(R.id.share_button)
    ImageView shareButton;
    @BindView(R.id.rating_text)
    TextView ratingText;
    @BindView(R.id.title_detail_maps)
    TextView titleDetailMaps;
    @BindView(R.id.street_detail_maps)
    TextView streetDetailMaps;
    @BindView(R.id.category_branch)
    RecyclerView categoryBranch;
    @BindView(R.id.img_overview)
    ImageView imgOverview;
    @BindView(R.id.overview_detail_maps)
    RelativeLayout overviewDetailMaps;
    @BindView(R.id.img_review)
    ImageView imgReview;
    @BindView(R.id.review_button)
    RelativeLayout reviewButton;
    @BindView(R.id.button_slide)
    LinearLayout buttonSlide;
    @BindView(R.id.frameDetailMaps)
    FrameLayout frameDetailMaps;
    @BindView(R.id.refill_here)
    Button refillHere;
    Context context;
    RequestInterfaces mReq;

    LayoutInflater inflater;
    View dialogView;
    AlertDialog.Builder dialog;
    String ID;
    Double longitude;
    Double lattitude;

    Double lon;
    Double latt;

    String pointErn, placeReff;

    @BindView(R.id.pinPremium)
    ImageView pinPremium;
    @BindView(R.id.count_reviews)
    TextView countReviews;
    @BindView(R.id.refillhere1)
    Button refillhere1;
    SessionManager sessionManager;



    ArrayList<TagBean> tag;
    AdapterCategory adapterCategory;

    private Runnable refresh;
    int ratings;


    private DividerItemDecoration dividerItemDecoration;
    Calendar calendar;
    public static String Nama, foto, detail, poin, premium, jambuka, jamtutup, status_buka, harga, type, alamat, id, url;
    int jml_reviews;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_maps);
        sessionManager = new SessionManager(this);
        ButterKnife.bind(this);


        context = this;
        mReq = Utils.getApiServices();


        LoadJson();
        Countreview();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getAddress();
                initComponent();
                handler.postDelayed(this, 0);
            }
        }, 200);





        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        categoryBranch.setHasFixedSize(true);
        categoryBranch.setLayoutManager(linearLayoutManager);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameDetailMaps, FragOverview.newInstance());
        transaction.commit();





    }

    private void initComponent() {


        if((lon >= (longitude-0.00030)) && (latt >= (lattitude-0.00030)) && (lon <= (longitude+0.00030)) && (latt <= (lattitude+0.00030))){
            refillhere1.setVisibility(View.VISIBLE);
            refillHere.setVisibility(View.GONE);
        } else {
            refillhere1.setVisibility(View.GONE);
            refillHere.setVisibility(View.VISIBLE);
        }

        refillHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                direction();
            }
        });
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareBut();
            }
        });
        refillhere1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRefill();
            }
        });


        backDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }



    public void getAddress() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationManager locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            longitude = location.getLongitude();
            lattitude = location.getLatitude();
//            Toast.makeText(DetailMaps.this, String.format("%.5f", longitude) + " " + String.format("%.5f", lattitude), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Null", Toast.LENGTH_SHORT).show();
        }



    }


    private void LoadJson() {

        ID = sessionManager.getSessionMapSelectDetail();

        mReq.getDetailMaps(ID).enqueue(new Callback<ResponseDetailsMaps>() {
            @Override
            public void onResponse(Call<ResponseDetailsMaps> call, Response<ResponseDetailsMaps> response) {
                if (response.isSuccessful()) {
                    ResponseDetailsMaps responseDetailsMaps = response.body();
                    if(responseDetailsMaps.getStatus().equals("200")){
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            calendar = Calendar.getInstance();
                            int CurrH = calendar.get(Calendar.HOUR_OF_DAY) + 7;

                            id = response.body().getData().get(i).getId().toString();

                            Nama = response.body().getData().get(i).getNama().toString();
                            alamat = response.body().getData().get(i).getAlamat().toString();
                            foto = response.body().getData().get(i).getFoto().toString();
                            premium = response.body().getData().get(i).getPremium().toString();
                            url = response.body().getData().get(i).getShaddr().toString();


                            Picasso.with(context).load(foto).into(picWallDetail);
                            titleDetailMaps.setText(Nama);
                            if (premium.equals("y")) {
                                pinPremium.setVisibility(View.VISIBLE);
                            } else {
                                pinPremium.setVisibility(View.GONE);
                            }
                            streetDetailMaps.setText(alamat);
                            ratingText.setText(responseDetailsMaps.getData().get(i).getStar().toString() + "/5");

                            tag = new ArrayList<>(Arrays.asList(responseDetailsMaps.getData().get(i).getTag()));
                            lon = Double.parseDouble(responseDetailsMaps.getData().get(i).getLongitude());
                            latt = Double.parseDouble(responseDetailsMaps.getData().get(i).getLatitude());

                        }
                    }




                    adapterCategory = new AdapterCategory(tag);
                    categoryBranch.setAdapter(adapterCategory);

                }

            }

            @Override
            public void onFailure(Call<ResponseDetailsMaps> call, Throwable t) {

            }
        });
    }


    public void Countreview() {
        mReq.getReview(sessionManager.getSessionMapSelectDetail().toString()).enqueue(new Callback<ModelReview>() {
            @Override
            public void onResponse(Call<ModelReview> call, Response<ModelReview> response) {
                ModelReview modelReview = response.body();
                if (modelReview.getData()==null) {
                    countReviews.setText("Reviews " + "(0)");
                } else {
                    countReviews.setText("Reviews " + "(" + response.body().getData().length + ")");
                }

            }

            @Override
            public void onFailure(Call<ModelReview> call, Throwable t) {

            }
        });
    }


    @OnClick({ R.id.overview_detail_maps, R.id.review_button})
    public void onViewClicked(View view) {
        Fragment selection = null;
        switch (view.getId()) {
            case R.id.overview_detail_maps:
                selection = new FragOverview();
                break;
            case R.id.review_button:
                selection = new FragReview();
                break;

        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameDetailMaps, selection);
        transaction.commit();
    }

    public void direction() {
        Uri IntentUri = Uri.parse("google.navigation:q=" + lattitude + "," + longitude + "&mode=d");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, IntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    public void customDialog(String show) {

        TextView placeDescription;
        final RatingBar ratingBar;
        final EditText comment;
        ImageView done;


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_dialog_rating);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

        ratingBar = (RatingBar) dialog.findViewById(R.id.rating_bar);
        placeDescription = (TextView) dialog.findViewById(R.id.text_description);
        comment = (EditText) dialog.findViewById(R.id.et_comment);

        done = (ImageView) dialog.findViewById(R.id.done_buttons);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratings = (int) rating;

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                mReq.PostReviewPlace(id, sessionManager.getSessionToken().toString(), String.valueOf(ratings), comment.getText().toString()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            Countreview();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });


        if(show.equals("TRUE")){
            dialog.show();
        } else {
            dialog.dismiss();
        }
    }


    public void dialogPoint(String point, String msg, final String stat) {

        ImageView done;


        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_dialog_refill);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

        TextView pointEarn = dialog.findViewById(R.id.earned_points);
        TextView placeRefill = dialog.findViewById(R.id.text_congrats);
        done = dialog.findViewById(R.id.done_button);
        pointEarn.setText(point);
        placeRefill.setText(msg);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                customDialog(stat);
            }
        });

        dialog.show();




    }

    public void shareBut(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "RefillMyBottle");
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share Place Link Via :"));
    }

    public void NotFound() {
        Fragment fragment = new NotFoundReview();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameDetailMaps, fragment);
        transaction.commit();
    }

    public void setRefill(){
       mReq.getRefill(sessionManager.getSessionToken(), sessionManager.getSessionMapSelectDetail()).enqueue(new Callback<responseRefill>() {
           @Override
           public void onResponse(Call<responseRefill> call, Response<responseRefill> response) {
               responseRefill responseRefill = response.body();
               if(response.isSuccessful()){

                       dialogPoint(responseRefill.getPts(), responseRefill.getMsg(), responseRefill.getReview());
                       sessionManager.saveSessionStr(sessionManager.SESSION_CURRENT_STAT_BOTTLE, sessionManager.getSessionMaxbottle());
                       sessionManager.saveSessionStr(sessionManager.SESSION_PERCENTAGE, "100");
               }
           }

           @Override
           public void onFailure(Call<responseRefill> call, Throwable t) {

           }
       });
    }


}

