package com.refillmybottle.refilmybottle.Maps.POJONearby;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class GeometryModels {
    @SerializedName("location")
    private LocationModels location;

    public static GeometryModels objectFromData(String str) {

        return new Gson().fromJson(str, GeometryModels.class);
    }

    public LocationModels getLocation() {
        return location;
    }

    public void setLocation(LocationModels location) {
        this.location = location;
    }
}
