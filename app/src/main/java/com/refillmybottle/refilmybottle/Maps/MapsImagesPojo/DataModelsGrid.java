package com.refillmybottle.refilmybottle.Maps.MapsImagesPojo;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class DataModelsGrid {

    /**
     * name : Alfamart Beruang
     * star : 5
     * latitude : -6.275587
     * longitude : 106.738757
     * img : http://192.168.1.20/htdocs/magang/asset/papic/1.jpg
     */

    @SerializedName("name")
    private String name;
    @SerializedName("star")
    private String star;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("img")
    private String img;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
