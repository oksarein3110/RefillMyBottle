package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressComponentsModelsAuto {
    @SerializedName("long_name")
    public String longName;
    @SerializedName("short_name")
    public String shortName;
    @SerializedName("types")
    public List<String> types;

    public static AddressComponentsModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, AddressComponentsModelsAuto.class);
    }
}
