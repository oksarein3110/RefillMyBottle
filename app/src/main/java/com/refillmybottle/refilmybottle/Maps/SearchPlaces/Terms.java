package com.refillmybottle.refilmybottle.Maps.SearchPlaces;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Terms implements Serializable {
    @SerializedName("offset")
    private int offset;
    @SerializedName("value")
    private String value;

    public int getOffset() {
        return offset;
    }

    public String getValue() {
        return value;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
