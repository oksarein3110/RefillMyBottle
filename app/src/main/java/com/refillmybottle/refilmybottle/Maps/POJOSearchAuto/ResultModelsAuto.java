package com.refillmybottle.refilmybottle.Maps.POJOSearchAuto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultModelsAuto {
    @SerializedName("adr_address")
    public String adrAddress;
    @SerializedName("formatted_address")
    public String formattedAddress;
    @SerializedName("formatted_phone_number")
    public String formattedPhoneNumber;
    @SerializedName("geometry")
    public GeometryModelsAuto geometry;
    @SerializedName("icon")
    public String icon;
    @SerializedName("id")
    public String id;
    @SerializedName("international_phone_number")
    public String internationalPhoneNumber;
    @SerializedName("name")
    public String name;
    @SerializedName("place_id")
    public String placeId;
    @SerializedName("rating")
    public double rating;
    @SerializedName("reference")
    public String reference;
    @SerializedName("scope")
    public String scope;
    @SerializedName("url")
    public String url;
    @SerializedName("utc_offset")
    public int utcOffset;
    @SerializedName("vicinity")
    public String vicinity;
    @SerializedName("website")
    public String website;
    @SerializedName("address_components")
    public List<AddressComponentsModelsAuto> addressComponents;
    @SerializedName("reviews")
    public List<ReviewsModelsAuto> reviews;
    @SerializedName("types")
    public List<String> types;

    public static ResultModelsAuto objectFromData(String str) {

        return new Gson().fromJson(str, ResultModelsAuto.class);
    }

    public GeometryModelsAuto getGeometry() {
        return geometry;
    }

    public String getName() {
        return name;
    }

    public String getAdrAddress() {
        return adrAddress;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public double getRating() {
        return rating;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public String getIcon() {
        return icon;
    }

    public String getId() {
        return id;
    }

    public int getUtcOffset() {
        return utcOffset;
    }

    public String getInternationalPhoneNumber() {
        return internationalPhoneNumber;
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getReference() {
        return reference;
    }

    public String getScope() {
        return scope;
    }

    public String getUrl() {
        return url;
    }

    public List<AddressComponentsModelsAuto> getAddressComponents() {
        return addressComponents;
    }

    public String getVicinity() {
        return vicinity;
    }

    public String getWebsite() {
        return website;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ReviewsModelsAuto> getReviews() {
        return reviews;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setAddressComponents(List<AddressComponentsModelsAuto> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public void setAdrAddress(String adrAddress) {
        this.adrAddress = adrAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public void setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
    }

    public void setGeometry(GeometryModelsAuto geometry) {
        this.geometry = geometry;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setInternationalPhoneNumber(String internationalPhoneNumber) {
        this.internationalPhoneNumber = internationalPhoneNumber;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setReviews(List<ReviewsModelsAuto> reviews) {
        this.reviews = reviews;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public void setUtcOffset(int utcOffset) {
        this.utcOffset = utcOffset;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
