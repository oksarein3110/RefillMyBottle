package com.refillmybottle.refilmybottle;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.refillmybottle.refilmybottle.Event.CalendarEvent;
import com.refillmybottle.refilmybottle.Maps.MapsShowing;
import com.refillmybottle.refilmybottle.Profile.ProfileHome;
import com.refillmybottle.refilmybottle.Shop.ListPopularShop;
import com.refillmybottle.refilmybottle.newrefill.FormRefill;
import com.refillmybottle.refilmybottle.newrefill.fragment_new_intro;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentParent extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.frame_layout)
    FrameLayout frameLayout;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_parent);
        ButterKnife.bind(this);

        BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
        bottomNavigation.setOnNavigationItemSelectedListener(this);

        Intent intent = getIntent();
        if(intent.hasExtra("code")){
            int intentFragment = getIntent().getExtras().getInt("frgToLoad");
            switch (intentFragment) {
                case 1:
                    loadFragment(FormRefill.newInstance());
                    break;
            }
        } else {
            loadFragment(MapsShowing.newInstance());
        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment=null;
        switch (item.getItemId()){
            case R.id.Maps :
                fragment = MapsShowing.newInstance();
                break;
            case R.id.New :
                fragment = fragment_new_intro.newInstance();
                break;
            case R.id.Event :
                fragment = CalendarEvent.newInstance();
                break;
            case R.id.Shop :
                fragment = ListPopularShop.newInstance();
                break;
            case R.id.Profile :
                fragment = ProfileHome.newInstance();
                break;
        }
        return loadFragment(fragment);
    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .commit();
            return true;
        }
        return false;
    }



}


