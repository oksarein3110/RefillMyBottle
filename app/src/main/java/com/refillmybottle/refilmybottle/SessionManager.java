package com.refillmybottle.refilmybottle;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by abah on 19/07/18.
 */

public class SessionManager {

    public static final String SESSION_REFILL = "sessionREFILL";
    public static final String SESSION_EMAIL = "sessionEMAIL";
    public static final String SESSION_NMDP = "sessionNMDP";
    public static final String SESSION_NMBK = "sessionNMBK";
    public static final String SESSION_PHOTO = "sessionFOTO";
    public static final String SESSION_DOB = "sessionDOB";
    public static final String SESSION_COUNTRY = "sessionCOUNTRY";
    public static final String SESSION_STATE = "sessionREGION";
    public static final String SESSION_CITY = "sessionCITY";
    public static final String SESSION_STREET = "sessionSTREET";
    public static final String SESSION_UPLOAD_IMAGE_REG = "sessionIMAGE_REG";
    public static final String SESSION_TOKEN = "sessionToken";
    public static final String SESSION_ID_FB = "sessionIDFB";
    public static final String SESSION_INTRO = "sessionIntro";
    public static final String SESSION_POIN = "sessionPoin";
    public static final String SESSION_STATUS = "sessionSTATUS";
    public static final String SESSION_PREDICT_BOTTLE_DAY = "sessionPredictBottleDay";
    public static final String SESSION_MAXBOTTLE = "sessionMaxBottle";
    public static final String SESSION_PERCENTAGE = "sessionPercentage";
    public static final String SESSION_CURRENT_STAT_BOTTLE = "sessionCurrStatBottle";
    public static final String SESSION_SELECTION_MARKER = "sessionSelectionMarker";
    public static final String SESSION_MAP_SELECT_DETAIL = "sessionMapsDetailID";
    public static final String SESSION_ID_SHOP = "sessionIDShop";
    public static final String SESSION_ID_EVENT = "sessionEvent";
    public static final String SESSION_TEMP_SELECTPLACE = "sessionTempIDSelP";
    public static final String SESSION_TEMP_POINT = "sessionTempPoin";
    public static final String SESSION_IDPLACE = "sessionIDPlace";
    public static final String SESSION_FB = "sessionFB";

    SharedPreferences session;
    SharedPreferences.Editor sessionEditor;

    public SessionManager(Context context){
        session = context.getSharedPreferences(SESSION_REFILL, Context.MODE_PRIVATE);
        sessionEditor = session.edit();
    }

    public void saveSessionStr(String keySP, String value){
        sessionEditor.putString(keySP, value);
        sessionEditor.commit();
    }
    public void saveSessionInt(String keySp, int value){
        sessionEditor.putInt(keySp, value);
        sessionEditor.commit();
    }
    public void saveSessionBoolean(String keySp, boolean value){
        sessionEditor.putBoolean(keySp, value);
        sessionEditor.commit();
    }

    public String getSessionNmbk() {
        return session.getString(SESSION_NMBK, "");
    }

    public String getSessionNmdp(){
        return session.getString(SESSION_NMDP, "");
    }

    public String getSessionEmail() {
        return session.getString(SESSION_EMAIL, "");
    }

    public String getSessionPhoto(){
        return session.getString(SESSION_PHOTO, "");
    }

    public String getSessionDob(){
        return session.getString(SESSION_DOB, "");
    }

    public String getSessionCountry(){
        return session.getString(SESSION_COUNTRY, "");
    }

    public String getSessionRegion(){
        return session.getString(SESSION_STATE, "");
    }

    public String getSessionCity(){
        return session.getString(SESSION_CITY, "");
    }

    public String getSessionStreet(){
        return session.getString(SESSION_STREET, "");
    }

    public boolean getSessionStatus(){
        return session.getBoolean(SESSION_STATUS, false);
    }

    public boolean getSessionIntro() {
        return session.getBoolean(SESSION_INTRO, false);
    }
    public String getSessionPoin(){return session.getString(SESSION_POIN, "");}

   public String getSessionMaxbottle(){return  session.getString(SESSION_MAXBOTTLE, "2.5");}
   public String getSessionPredictBottleDay(){return session.getString(SESSION_PREDICT_BOTTLE_DAY, "");}
   public String getSessionCurrentStatBottle(){return session.getString(SESSION_CURRENT_STAT_BOTTLE, "0");}
   public String getSessionPercentage(){return session.getString(SESSION_PERCENTAGE, "0");}

    public String getSessionUploadImageReg() {
        return session.getString(SESSION_UPLOAD_IMAGE_REG, "");
    }

    public String getSessionIdplace(){return  session.getString(SESSION_IDPLACE, "");}
    public String getSessionTempSelectplace(){
        return session.getString(SESSION_TEMP_SELECTPLACE, "");
    }

    public String getSessionTempPoint(){
        return session.getString(SESSION_TEMP_POINT, "");
    }

    public String getSessionToken(){
        return session.getString(SESSION_TOKEN, "");
    }

    public String getSessionMapSelectDetail(){
        return  session.getString(SESSION_MAP_SELECT_DETAIL, "");
    }

    public String getSessionIdShop(){
        return session.getString(SESSION_ID_SHOP, "");
    }

    public String getSessionIdEvent(){
        return session.getString(SESSION_ID_EVENT, "");
    }

    public String getSessionIdFb(){
        return session.getString(SESSION_ID_FB,"");
    }

    public String getSessionFb(){
        return session.getString(SESSION_FB, "");
    }

}
