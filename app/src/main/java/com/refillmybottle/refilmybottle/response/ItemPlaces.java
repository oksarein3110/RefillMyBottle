package com.refillmybottle.refilmybottle.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abah on 03/08/18.
 */

public class ItemPlaces {
    @SerializedName("id")
    private String id;
    @SerializedName("nama")
    private String nama;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("mark")
    private String mark;
    @SerializedName("star")
    private String star;



    public void setId(String id) {
        this.id = id;
    }


    public void setNama(String nama) {
        this.nama = nama;
    }



    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getStar() {
        return star;
    }

    public String getNama() {
        return nama;
    }

    public String getId() {
        return id;
    }


    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getMark() {
        return mark;
    }



}

