package com.refillmybottle.refilmybottle.response;

import com.google.gson.annotations.SerializedName;

public class UserBeanXX {
    @SerializedName("token")
    private String token;
    @SerializedName("email")
    private String email;
    @SerializedName("photo")
    private String photo;
    @SerializedName("namadpn")
    private String namadpn;
    @SerializedName("namablk")
    private String namablk;
    @SerializedName("tgl")
    private String tgl;
    @SerializedName("negara")
    private String negara;
    @SerializedName("prov")
    private String prov;
    @SerializedName("kota")
    private String kota;
    @SerializedName("jln")
    private String jln;
    @SerializedName("poin")
    private String poin;
    @SerializedName("progress")
    private String progress;
    @SerializedName("date")
    private String date;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNamadpn() {
        return namadpn;
    }

    public void setNamadpn(String namadpn) {
        this.namadpn = namadpn;
    }

    public String getNamablk() {
        return namablk;
    }

    public void setNamablk(String namablk) {
        this.namablk = namablk;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getNegara() {
        return negara;
    }

    public void setNegara(String negara) {
        this.negara = negara;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getJln() {
        return jln;
    }

    public void setJln(String jln) {
        this.jln = jln;
    }

    public String getPoin() {
        return poin;
    }

    public void setPoin(String poin) {
        this.poin = poin;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
