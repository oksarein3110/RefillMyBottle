package com.refillmybottle.refilmybottle.response;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class LonlatModels {
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;

    public static LonlatModels objectFromData(String str) {

        return new Gson().fromJson(str, LonlatModels.class);
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
