package com.refillmybottle.refilmybottle.response;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataModels {
    @SerializedName("name")
    private String name;
    @SerializedName("lonlat")
    private LonlatModels lonlat;
    @SerializedName("star")
    private String star;
    @SerializedName("image")
    private List<String> image;

    public static DataModels objectFromData(String str) {

        return new Gson().fromJson(str, DataModels.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LonlatModels getLonlat() {
        return lonlat;
    }

    public void setLonlat(LonlatModels lonlat) {
        this.lonlat = lonlat;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }
}
