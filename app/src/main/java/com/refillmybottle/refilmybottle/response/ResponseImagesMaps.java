package com.refillmybottle.refilmybottle.response;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abah on 26/08/18.
 */

public class ResponseImagesMaps {
   @SerializedName("status")
    private String status;
   @SerializedName("data")
    private List<DataModels> data;
   @SerializedName("msg")
    private String  msg;

    public void setData(List<DataModels> data) {
        this.data = data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DataModels> getData() {
        return data;
    }

    public String getMsg() {
        return msg;
    }

    public String getStatus() {
        return status;
    }
}
