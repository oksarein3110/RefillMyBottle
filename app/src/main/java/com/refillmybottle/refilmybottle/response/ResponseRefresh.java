package com.refillmybottle.refilmybottle.response;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseRefresh {

    /**
     * msg : OK
     * user : {"email":"dandyoctavian@gmail.com","photo":"http://192.168.1.20/htdocs/magang/asset/uspic/54af8ce8c05a20e8229c70c0f6eb4a38.jpeg","namadpn":"Dandy","namablk":"Oktafian","tgl":"1998-10-10","negara":"Indonesia","prov":"162","kota":"Cirebon","jln":"Jl. R.A. Kartini No. 10","poin":"192","progress":"162"}
     * status : 200
     */

    @SerializedName("msg")
    private String msg;
    @SerializedName("user")
    private UserBeanX user;
    @SerializedName("status")
    private String status;

    public static ResponseRefresh objectFromData(String str, String key) {

        try {
            JSONObject jsonObject = new JSONObject(str);

            return new Gson().fromJson(jsonObject.getString(str), ResponseRefresh.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserBeanX getUser() {
        return user;
    }

    public void setUser(UserBeanX user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
