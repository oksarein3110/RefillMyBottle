package com.refillmybottle.refilmybottle.response;

import com.google.gson.annotations.SerializedName;

public class ResponseFB {

    @SerializedName("msg")
    private String msg;
    @SerializedName("user")
    private UserBeanXX user;
    @SerializedName("status")
    private String status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserBeanXX getUser() {
        return user;
    }

    public void setUser(UserBeanXX user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
