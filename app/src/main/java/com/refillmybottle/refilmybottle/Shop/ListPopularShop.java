package com.refillmybottle.refilmybottle.Shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.RetrofitClient;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListPopularShop extends Fragment {

    @BindView(R.id.see_all)
    TextView seeAll;
    @BindView(R.id.recycler_popularshop)
    RecyclerView recyclerPopularshop;
    Unbinder unbinder;
    RequestInterfaces mRequestInterfaces;


    private ArrayList<ModelShopPopular> data_popular;
    private AdapterPopularShop adapterPopularShop;

    public static ListPopularShop newInstance() {
        ListPopularShop listPopularShop = new ListPopularShop();
        return listPopularShop;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_list_popular_shop, container, false);
        unbinder = ButterKnife.bind(this, view);
        mRequestInterfaces = Utils.getApiServices();
        initView();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.see_all})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.see_all:
                startActivity(new Intent(getContext(), AllProductShop.class));
                break;
        }
    }

    private void initView(){
        recyclerPopularshop.setHasFixedSize(true);
        recyclerPopularshop.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        loadJSON();
    }

    private void loadJSON() {
        mRequestInterfaces.getItemPopular().enqueue(new Callback<ResponsePopular>() {
            @Override
            public void onResponse(Call<ResponsePopular> call, Response<ResponsePopular> response) {
                if(response.isSuccessful()){
                    ResponsePopular responsePopular = response.body();
                    data_popular = new ArrayList<>(Arrays.asList(responsePopular.getData()));
                    adapterPopularShop = new AdapterPopularShop(data_popular);
                    recyclerPopularshop.setAdapter(adapterPopularShop);
                }


            }

            @Override
            public void onFailure(Call<ResponsePopular> call, Throwable t) {

            }
        });


    }

}
