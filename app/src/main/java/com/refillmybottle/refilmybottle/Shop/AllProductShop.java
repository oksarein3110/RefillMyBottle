package com.refillmybottle.refilmybottle.Shop;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllProductShop extends AppCompatActivity {


    RequestInterfaces mRequestInterfaces;
    @BindView(R.id.recycler_all)
    RecyclerView recyclerAll;

    private ArrayList<ModelShopPopular> data_popular;
    private AdapterPopularShop adapterPopularShop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_product_shop);
        ButterKnife.bind(this);
        mRequestInterfaces = Utils.getApiServices();
        initView();

    }


    private void initView() {
        recyclerAll.setHasFixedSize(true);
        recyclerAll.setLayoutManager(new GridLayoutManager(this, 2));
        loadJSON();

    }

    private void loadJSON() {
        mRequestInterfaces.getItemShop().enqueue(new Callback<ResponsePopular>() {
            @Override
            public void onResponse(Call<ResponsePopular> call, Response<ResponsePopular> response) {
                ResponsePopular responsePopular = response.body();
                data_popular = new ArrayList<>(Arrays.asList(responsePopular.getData()));
                adapterPopularShop = new AdapterPopularShop(data_popular);
                recyclerAll.setAdapter(adapterPopularShop);
            }

            @Override
            public void onFailure(Call<ResponsePopular> call, Throwable t) {

            }
        });
    }
}
