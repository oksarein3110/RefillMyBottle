package com.refillmybottle.refilmybottle.Shop;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.ResponseDetailShop;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentHowtoUse extends Fragment {

    @BindView(R.id.tx_howto)
    TextView txHowto;
    Unbinder unbinder;
    SessionManager sessionManager;
    RequestInterfaces requestInterfaces;
    public FragmentHowtoUse() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment_howto_use, container, false);
        unbinder = ButterKnife.bind(this, view);
        sessionManager = new SessionManager(getContext());
        requestInterfaces = Utils.getApiServices();

        requestInterfaces.getDetailShop(sessionManager.getSessionIdShop(), sessionManager.getSessionToken()).enqueue(new Callback<ResponseDetailShop>() {
            @Override
            public void onResponse(Call<ResponseDetailShop> call, Response<ResponseDetailShop> response) {
                if(response.isSuccessful()){
                    ResponseDetailShop responseDetailShop = response.body();
                    for (int i=0; i < responseDetailShop.getData().size(); i++){
                        txHowto.setText(responseDetailShop.getData().get(i).getCaraGuna().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseDetailShop> call, Throwable t) {

            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
