package com.refillmybottle.refilmybottle.Shop;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.ResponseDetailShop;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentTC extends Fragment {


    Unbinder unbinder;

    RequestInterfaces mReq;


    SessionManager sessionManager;
    String id;
    @BindView(R.id.txtTnc)
    TextView txtTnc;


    public FragmentTC() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment_tc, container, false);
        unbinder = ButterKnife.bind(this, view);
        mReq = Utils.getApiServices();
        Bundle arg = getArguments();
        sessionManager = new SessionManager(getContext());


        id = sessionManager.getSessionIdShop();

        LoadJSON();
        return view;
    }

    private void LoadJSON() {
        mReq.getDetailShop(sessionManager.getSessionIdShop(), sessionManager.getSessionToken()).enqueue(new Callback<ResponseDetailShop>() {
            @Override
            public void onResponse(Call<ResponseDetailShop> call, Response<ResponseDetailShop> response) {
                ResponseDetailShop responseDetailShop = response.body();
                if(responseDetailShop.getStatus().equals("200")){
                    for(int i=0; i < responseDetailShop.getData().size(); i++){
                        txtTnc.setText(responseDetailShop.getData().get(i).getTnc().toString());
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseDetailShop> call, Throwable throwable) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
