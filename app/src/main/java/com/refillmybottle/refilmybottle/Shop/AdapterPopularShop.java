package com.refillmybottle.refilmybottle.Shop;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by abah on 31/07/18.
 */

public class AdapterPopularShop extends RecyclerView.Adapter<AdapterPopularShop.ViewHolder> {

    private Context context;
    private ArrayList<ModelShopPopular> data;
    private SessionManager sessionManager;

    public AdapterPopularShop(ArrayList<ModelShopPopular> data) {
        this.data = data;
    }

    @Override
    public AdapterPopularShop.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_shop, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterPopularShop.ViewHolder holder, final int position) {

      final String id = data.get(position).getId().toString();
      holder.brand_item.setText(data.get(position).getNama().toString());
      holder.disc_item.setText(data.get(position).getHargaPoin().toString());
      holder.price_item.setText(data.get(position).getHarga().toString());
      holder.item_point.setText(data.get(position).getAmbilPoin().toString());
        holder.card_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailShop.class);
                sessionManager.saveSessionStr(sessionManager.SESSION_ID_SHOP, "");
                sessionManager.saveSessionStr(sessionManager.SESSION_ID_SHOP, data.get(position).getId().toString());
                context.startActivity(intent);
            }
        });
        Picasso.with(context)
                .load(data.get(position).getFoto().toString())
                .resize(150, 150)
                .into(holder.image_item);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView brand_item, price_item,item_point, disc_item;
        private CardView card_shop;
        private ImageView image_item;
        public ViewHolder(View view){
            super (view);
            context = view.getContext();
            sessionManager = new SessionManager(view.getContext());

             image_item = view.findViewById(R.id.image_item_shop);
             brand_item = view.findViewById(R.id.brand_item);
             price_item = view.findViewById(R.id.price_item);
             item_point = view.findViewById(R.id.item_points);
             disc_item = view.findViewById(R.id.disc_item);
             card_shop = view.findViewById(R.id.card_shop_popular);
        }
    }
}
