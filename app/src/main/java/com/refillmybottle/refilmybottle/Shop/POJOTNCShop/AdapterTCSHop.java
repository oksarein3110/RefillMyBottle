package com.refillmybottle.refilmybottle.Shop.POJOTNCShop;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;

import java.util.ArrayList;

public class AdapterTCSHop extends RecyclerView.Adapter<AdapterTCSHop.ViewHolder> {

    private ArrayList<DataModelsTCShop> data;

    public AdapterTCSHop(ArrayList<DataModelsTCShop> data){
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterTCSHop.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reviews_places, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterTCSHop.ViewHolder holder, int position) {
        holder.username.setText(data.get(position).getSurel().toString());
        holder.date.setText(data.get(position).getDatetime().toString());
        holder.stars.setRating(Float.parseFloat(data.get(position).getBintang().toString()));
        holder.comment.setText(data.get(position).getKomentar().toString());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView date, username, comment;
        private RatingBar stars;
        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.dateTimeReview);
            username = itemView.findViewById(R.id.UserNameName);
            comment = itemView.findViewById(R.id.commentReviewMaps);
            stars = itemView.findViewById(R.id.ratingPlaceReview);
        }
    }
}
