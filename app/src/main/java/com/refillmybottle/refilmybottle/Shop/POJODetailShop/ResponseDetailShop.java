package com.refillmybottle.refilmybottle.Shop.POJODetailShop;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseDetailShop {


    @SerializedName("status")
    private String status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private List<DataModelsShop> data;

    public static ResponseDetailShop objectFromData(String str) {

        return new Gson().fromJson(str, ResponseDetailShop.class);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataModelsShop> getData() {
        return data;
    }

    public void setData(List<DataModelsShop> data) {
        this.data = data;
    }
}
