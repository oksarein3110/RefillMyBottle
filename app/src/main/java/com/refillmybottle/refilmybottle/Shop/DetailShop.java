package com.refillmybottle.refilmybottle.Shop;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rd.PageIndicatorView;
import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.DataModelsShop;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.ImagesShop;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.ResponseDetailShop;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailShop extends AppCompatActivity {
    @BindView(R.id.text_product_name)
    TextView textProductName;
    @BindView(R.id.points_bottle)
    TextView pointsBottle;
    @BindView(R.id.purchase_button)
    TextView purchaseButton;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.view_pager_details)
    ViewPager viewPagerDetails;
    @BindView(R.id.backShop)
    ImageView backShop;
    @BindView(R.id.coins)
    ImageView coins;
    @BindView(R.id.expire_status)
    TextView expireStatus;
    @BindView(R.id.relative_content)
    RelativeLayout relativeContent;
    @BindView(R.id.viewPagerPict)
    ViewPager viewPagerPict;
    @BindView(R.id.IndicatorSlideShop)
    PageIndicatorView IndicatorSlideShop;

    private static int currentPages = 0;
    private static int NUM_PAGES = 0;

    AdapterImagesSlider adapterImagesSlider;
    ArrayList<ImagesShop> urlImages;

    private String nama_brand, foto, point, overview, howto, tnc, url, id;
    RequestInterfaces mReq;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_shop);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        mReq = Utils.getApiServices();

        LoadJson();


        SimpleFragmentParent adapter = new SimpleFragmentParent(this, getSupportFragmentManager());
        viewPagerDetails.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPagerDetails);

        backShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }



    private void LoadJson() {
        mReq.getDetailShop(sessionManager.getSessionIdShop().toString(), sessionManager.getSessionToken().toString()).enqueue(new Callback<ResponseDetailShop>() {
            @Override
            public void onResponse(Call<ResponseDetailShop> call, Response<ResponseDetailShop> response) {
                ResponseDetailShop responseDetailShop = response.body();
                if (response.isSuccessful()) {
                    for (int i = 0; i < responseDetailShop.getData().size(); i++) {
                        nama_brand = responseDetailShop.getData().get(i).getNama().toString();
                        foto = responseDetailShop.getData().get(i).getFoto().toString();
                        point = responseDetailShop.getData().get(i).getAmbilPoin().toString();
                        url = responseDetailShop.getData().get(i).getUrl().toString();
                        overview = responseDetailShop.getData().get(i).getDetail().toString();
                        howto = responseDetailShop.getData().get(i).getCaraGuna().toString();
                        tnc = responseDetailShop.getData().get(i).getId().toString();

                        for(int j=0; j < responseDetailShop.getData().get(i).getImage().length; j++){
                            urlImages = new ArrayList<>(Arrays.asList(responseDetailShop.getData().get(i).getImage()));
                            adapterImagesSlider = new AdapterImagesSlider(DetailShop.this,urlImages);
                            viewPagerPict.setAdapter(adapterImagesSlider);
                        }


                    }




                    textProductName.setText(nama_brand);
                    pointsBottle.setText(point + " points");


                }
            }

            @Override
            public void onFailure(Call<ResponseDetailShop> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.purchase_button)
    public void onViewClicked() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    class SimpleFragmentParent extends FragmentPagerAdapter {
        private Context mContext;

        public SimpleFragmentParent(Context context, FragmentManager fm) {
            super(fm);
            mContext = context;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Fragment fragment = new FragmentOverview();
                Bundle args = new Bundle();
                args.putString("over", overview);
                fragment.setArguments(args);
                return fragment;
            } else if (position == 1) {
                Fragment fragment1 = new FragmentHowtoUse();
                Bundle args1 = new Bundle();
                args1.putString("htu", howto);
                fragment1.setArguments(args1);
                return fragment1;
            } else {
                Fragment fragment2 = new FragmentTC();
                Bundle args2 = new Bundle();
                args2.putString("tc", tnc);
                fragment2.setArguments(args2);
                return fragment2;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return mContext.getString(R.string.overview_title);
                case 1:
                    return mContext.getString(R.string.howto);
                case 2:
                    return mContext.getString(R.string.tc_title);
                default:
                    return null;
            }
        }
    }


}
