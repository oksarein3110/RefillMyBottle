package com.refillmybottle.refilmybottle.Shop.POJODetailShop;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataModelsShop {
    @SerializedName("id")
    private String id;
    @SerializedName("nama")
    private String nama;
    @SerializedName("detail")
    private String detail;
    @SerializedName("url")
    private String url;
    @SerializedName("harga")
    private String harga;
    @SerializedName("harga_poin")
    private String hargaPoin;
    @SerializedName("ambil_poin")
    private String ambilPoin;
    @SerializedName("cara_guna")
    private String caraGuna;
    @SerializedName("exp")
    private String exp;
    @SerializedName("foto")
    private String foto;
    @SerializedName("tnc")
    private String tnc;

    private ImagesShop[] image;

    public ImagesShop[] getImage() {
        return image;
    }



    public void setImage(ImagesShop[] image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getUrl() {
        return url;
    }

    public void setTnc(String tnc) {
        this.tnc = tnc;
    }

    public String getTnc() {
        return tnc;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getHargaPoin() {
        return hargaPoin;
    }

    public void setHargaPoin(String hargaPoin) {
        this.hargaPoin = hargaPoin;
    }

    public String getAmbilPoin() {
        return ambilPoin;
    }

    public void setAmbilPoin(String ambilPoin) {
        this.ambilPoin = ambilPoin;
    }

    public String getCaraGuna() {
        return caraGuna;
    }

    public void setCaraGuna(String caraGuna) {
        this.caraGuna = caraGuna;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }


}
