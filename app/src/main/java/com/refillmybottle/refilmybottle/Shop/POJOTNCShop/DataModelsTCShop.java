package com.refillmybottle.refilmybottle.Shop.POJOTNCShop;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class DataModelsTCShop {
    @SerializedName("id")
    private String id;
    @SerializedName("id_jual")
    private String idJual;
    @SerializedName("surel")
    private String surel;
    @SerializedName("bintang")
    private String bintang;
    @SerializedName("komentar")
    private String komentar;
    @SerializedName("datetime")
    private String datetime;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdJual() {
        return idJual;
    }

    public void setIdJual(String idJual) {
        this.idJual = idJual;
    }

    public String getSurel() {
        return surel;
    }

    public void setSurel(String surel) {
        this.surel = surel;
    }

    public String getBintang() {
        return bintang;
    }

    public void setBintang(String bintang) {
        this.bintang = bintang;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
