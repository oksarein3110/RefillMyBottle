package com.refillmybottle.refilmybottle.Shop;

import android.content.Context;
import android.media.Image;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.DataModelsShop;
import com.refillmybottle.refilmybottle.Shop.POJODetailShop.ImagesShop;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterImagesSlider extends PagerAdapter {

    private ArrayList<ImagesShop> urlImages;
    private LayoutInflater inflater;
    private Context context;

    public AdapterImagesSlider(Context context, ArrayList<ImagesShop> urlImages){
        this.context = context;
        this.urlImages = urlImages;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return urlImages.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View imageLayout = inflater.inflate(R.layout.images_layout, container, false);

        assert imageLayout !=null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.imagesContent);
        final String urls = urlImages.get(position).getImg();
        Picasso.with(context).load(urls)
                .into(imageView);
        container.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Nullable
    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void restoreState(@Nullable Parcelable state, @Nullable ClassLoader loader) {

    }
}
