package com.refillmybottle.refilmybottle.Shop.POJODetailShop;

import com.google.gson.annotations.SerializedName;

public class ImagesShop {
    @SerializedName("img")
    private String img;

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }
}
