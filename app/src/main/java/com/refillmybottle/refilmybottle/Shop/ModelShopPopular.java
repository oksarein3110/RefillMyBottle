package com.refillmybottle.refilmybottle.Shop;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abah on 31/07/18.
 */

public class ModelShopPopular {

    @SerializedName("id")
    private String id;
    @SerializedName("nama")
    private String nama;
    @SerializedName("harga")
    private String harga;
    @SerializedName("harga_poin")
    private String hargaPoin;
    @SerializedName("ambil_poin")
    private String ambilPoin;
    @SerializedName("foto")
    private String foto;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getHargaPoin() {
        return hargaPoin;
    }

    public void setHargaPoin(String hargaPoin) {
        this.hargaPoin = hargaPoin;
    }

    public String getAmbilPoin() {
        return ambilPoin;
    }

    public void setAmbilPoin(String ambilPoin) {
        this.ambilPoin = ambilPoin;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
