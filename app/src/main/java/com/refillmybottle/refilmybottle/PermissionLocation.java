package com.refillmybottle.refilmybottle;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PermissionLocation extends Fragment {

    Unbinder unbinder;
    Context mContext;

    public PermissionLocation newInstance() {
        PermissionLocation permissionLocation = new PermissionLocation();
        return permissionLocation;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_permission_location, container, false);
        unbinder = ButterKnife.bind(this, view);
        mContext = getContext();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.only_apps, R.id.always_allow, R.id.dont_allow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.only_apps:
                break;
            case R.id.always_allow:
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                break;
            case R.id.dont_allow:
                break;
        }
    }


}
