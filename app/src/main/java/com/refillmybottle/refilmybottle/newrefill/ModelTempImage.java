package com.refillmybottle.refilmybottle.newrefill;

import java.util.List;

public class ModelTempImage {
    private List<String> img;

    public List<String> getImg() {
        return img;
    }

    public void setImg(List<String> img) {
        this.img = img;
    }
}
