package com.refillmybottle.refilmybottle.newrefill;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.refillmybottle.refilmybottle.R;
import com.robertlevonyan.views.chip.Chip;
import com.robertlevonyan.views.chip.OnCloseClickListener;

import java.util.ArrayList;
import java.util.List;

public class AdapterImagesChips  extends RecyclerView.Adapter<AdapterImagesChips.ViewHolder>{

    private ArrayList<String> TempImages;
    int idx;
    public AdapterImagesChips (ArrayList TempImages){
        this.TempImages = TempImages;
    }

    @NonNull
    @Override
    public AdapterImagesChips.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowmultipleimage, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterImagesChips.ViewHolder holder, final int position) {
        //holder.chipss.setText(TempImages.indexOf(TempImages.get(position).toString()));

        holder.chipss.setOnCloseClickListener(new OnCloseClickListener() {
            @Override
            public void onCloseClick(View v) {
                TempImages.remove(TempImages.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return TempImages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView index;
        private Chip chipss;
        public ViewHolder(View itemView) {
            super(itemView);
            chipss = itemView.findViewById(R.id.chipsMaterial);
            index = itemView.findViewById(R.id.indexImageNum);
        }
    }
}
