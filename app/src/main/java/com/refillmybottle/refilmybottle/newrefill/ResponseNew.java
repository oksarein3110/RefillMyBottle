package com.refillmybottle.refilmybottle.newrefill;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ResponseNew {

    @SerializedName("status")
    private String status;
    @SerializedName("msg")
    private String msg;

    public static ResponseNew objectFromData(String str) {

        return new Gson().fromJson(str, ResponseNew.class);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
