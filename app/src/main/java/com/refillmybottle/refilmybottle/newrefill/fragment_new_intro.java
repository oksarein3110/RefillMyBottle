package com.refillmybottle.refilmybottle.newrefill;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import android.location.LocationListener;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Marker;
import com.refillmybottle.refilmybottle.Maps.MapsShowing;
import com.refillmybottle.refilmybottle.PermissionLocation;
import com.refillmybottle.refilmybottle.R;

import org.xml.sax.Locator;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class fragment_new_intro extends Fragment {
    RelativeLayout New;
    Fragment fragment;
    @BindView(R.id.newRefill)
    RelativeLayout newRefill;
    Unbinder unbinder;
    private String address, city, state;
    List<Address> addresses;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    LocationManager locationManager;
    private LocationListener locationListener;
    private GoogleApiClient mGoogleApiClient;
    private Marker mCurrentLocationMarker;

    private static final long MIN_DISTANCE = 1;
    private static final long MIN_TIME = 1;

    public fragment_new_intro() {

    }

    public static fragment_new_intro newInstance() {
        fragment_new_intro fragment_new_intro = new fragment_new_intro();
        return fragment_new_intro;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment_new_intro, container, false);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {

        }

        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.newRefill)
    public void onViewClicked() {
        FormRefill formRefill = FormRefill.newInstance();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().addToBackStack(null);
        fragmentTransaction.replace(R.id.frame_layout, formRefill);
        fragmentTransaction.commit();
    }


}
