package com.refillmybottle.refilmybottle.newrefill;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.refillmybottle.refilmybottle.R;
import com.refillmybottle.refilmybottle.ServicesHandler.RequestInterfaces;
import com.refillmybottle.refilmybottle.ServicesHandler.Utils;
import com.refillmybottle.refilmybottle.SessionManager;
import com.refillmybottle.refilmybottle.response.ItemRelation;
import com.refillmybottle.refilmybottle.response.ItemTypes;
import com.refillmybottle.refilmybottle.response.RelationResponse;
import com.refillmybottle.refilmybottle.response.TypesResponse;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormRefill extends Fragment {

    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.suggested_station)
    Spinner suggestedStation;
    @BindView(R.id.type_refill_station)
    Spinner typeRefillStation;
    @BindView(R.id.submit_type)
    Button submitType;
    Unbinder unbinder;
    Geocoder geocoder;
    List<Address> addresses;
    float dista = Float.parseFloat("0.1");
    ModelTempImage modelTempImage;
    String address, nama;

    ArrayList<String> TempImages;
    SessionManager sessionManager;


    Double longitude;
    Double lattitude;
    Context mContext;
    RequestInterfaces mBaseApi;
    @BindView(R.id.recyclerImageList)
    RecyclerView recyclerImageList;
    @BindView(R.id.et_NamePlaces)
    EditText etNamePlaces;
    @BindView(R.id.captureImage)
    ImageView captureImage;
    private Uri outputFileUri;
    String bestProvider;
    Criteria criteria;
    private static int PIC_CODE = 0;
    private static int PIC_REQUEST = 3;
    private static int TAKE_PICTURE = 1;

    LayoutInflater inflater;
    View dialogView;
    AlertDialog.Builder dialog;

    public static FormRefill newInstance() {
        FormRefill formRefill = new FormRefill();
        return formRefill;
    }

    public FormRefill() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_form_refill, container, false);
        unbinder = ButterKnife.bind(this, view);
        mBaseApi = Utils.getApiServices();
        mContext = getActivity();
        TempImages = new ArrayList<String>();
        sessionManager = new SessionManager(getContext());

        getAddress();
        initSpinnerRelation();
        initSpinnerTypes();

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        suggestedStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        typeRefillStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TakeFoto();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayout.HORIZONTAL);
        recyclerImageList.setLayoutManager(linearLayoutManager);
        recyclerImageList.setHasFixedSize(true);
        AdapterImagesChips adapterImagesChips = new AdapterImagesChips(TempImages);
        recyclerImageList.setAdapter(adapterImagesChips);


        if (TempImages.size() > 3) {
            submitType.setEnabled(false);
        } else {
            submitType.setEnabled(true);
        }

        return view;
    }


    public void getAddress() {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationManager locationManager = (LocationManager) getContext().getSystemService(getContext().LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            longitude = location.getLongitude();
            lattitude = location.getLatitude();
        } else {
            Toast.makeText(getContext(), "Null", Toast.LENGTH_SHORT).show();
        }

        geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lattitude, longitude, 1);
            address = addresses.get(0).getAddressLine(0);
            nama = addresses.get(0).getFeatureName().toString();
            Toast.makeText(getContext(), address + "   " + nama, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void initSpinnerRelation() {
        mBaseApi.getRelationData().enqueue(new Callback<RelationResponse>() {
            @Override
            public void onResponse(Call<RelationResponse> call, Response<RelationResponse> response) {
                if (response.isSuccessful()) {
                    List<ItemRelation> itemRespons = response.body().getData();
                    List<String> listSpinRel = new ArrayList<String>();
                    for (int i = 0; i < itemRespons.size(); i++) {
                        listSpinRel.add(itemRespons.get(i).getNama());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listSpinRel);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    suggestedStation.setAdapter(adapter);
                } else {
                    Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RelationResponse> call, Throwable t) {

            }
        });

    }

    private void initSpinnerTypes() {
        mBaseApi.getTypesData().enqueue(new Callback<TypesResponse>() {
            @Override
            public void onResponse(Call<TypesResponse> call, Response<TypesResponse> response) {
                if (response.isSuccessful()) {
                    List<ItemTypes> typesResponse = response.body().getData();
                    List<String> listSpinTypes = new ArrayList<String>();
                    for (int i = 0; i < typesResponse.size(); i++) {
                        listSpinTypes.add(typesResponse.get(i).getNama());
                    }
                    ArrayAdapter<String> aadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listSpinTypes);
                    aadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    typeRefillStation.setAdapter(aadapter);
                }
            }

            @Override
            public void onFailure(Call<TypesResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({ R.id.submit_type})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.submit_type:
                sendNewPlace();
                break;
        }
    }

    private void sendNewPlace() {
        mBaseApi.newPlace(etNamePlaces.getText().toString(), address, suggestedStation.getSelectedItem().toString(), typeRefillStation.getSelectedItem().toString(), String.valueOf(longitude), String.valueOf(lattitude), sessionManager.getSessionToken().toString(), TempImages).enqueue(new Callback<ResponseNew>() {
            @Override
            public void onResponse(Call<ResponseNew> call, Response<ResponseNew> response) {
                ResponseNew responseNew = response.body();
                if (response.isSuccessful()) {
                    if (responseNew.getStatus().equals("200")) {
                        Toast.makeText(getContext(), responseNew.getMsg().toString(), Toast.LENGTH_SHORT).show();
                        DialogSuccess();
                    } else {
                        Toast.makeText(getContext(), responseNew.getMsg().toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseNew> call, Throwable t) {

            }
        });

    }

    private void DialogSuccess() {
        dialog = new AlertDialog.Builder(getContext());
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.newrefilldialog, null);
        dialog.setView(dialogView);

        TextView ok = (TextView) dialogView.findViewById(R.id.okButtDialog);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }
        });

        dialog.show();
    }

    public void TakeFoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        outputFileUri = getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(intent, TAKE_PICTURE);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TAKE_PICTURE) {
            if (resultCode == 0) {
                return;
            }

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), outputFileUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            TempImages.add(encodeFromString(bitmap));

            if (TempImages.size() < 3) {
                TakeFoto();
            } else {

                Toast.makeText(getContext(), "Maximum Reached !", Toast.LENGTH_SHORT).show();
            }


        }
    }


    public static String encodeFromString(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 40, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.DEFAULT);
    }


}
