package com.refillmybottle.refilmybottle;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.refillmybottle.refilmybottle.Event.CalendarEvent;

import com.refillmybottle.refilmybottle.Intro.IntroMaster;


public class SplashScreen extends AppCompatActivity {
    private static int TIME_OUT_SPLASH = 3000;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sessionManager  = new SessionManager(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (sessionManager.getSessionIntro() == false){
                    startActivity(new Intent(SplashScreen.this, IntroMaster.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                } else {
                    startActivity(new Intent(SplashScreen.this, Login.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                }

            }
        }, TIME_OUT_SPLASH);
    }

}
